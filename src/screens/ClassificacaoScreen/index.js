import * as React from 'react';
import {connect} from 'react-redux';
import {Container, Content, Text, Tab, Tabs, Icon} from 'native-base';
import {View, TouchableHighlight, RefreshControl} from 'react-native';
import styles from './styles';
import BolaoHeader from '../../components/BolaoHeader';
import RodadaSelect from '../../components/RodadaSelect';
import {carregar_classificacao, carregar_classificacao_rodada, carregar_rodada} from '../../redux/actions/common';
import {bindActionCreators} from "redux";
import {addFsmToken} from "../../redux/actions/auth";

class TableHeaderSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showStart: 0,
            showStop: 3,
        };
    }

    proximo() {
        const nextStart = this.state.showStart + 1;
        const nextStop = this.state.showStop + 1;
        if (nextStop > 7) {
            return;
        }
        this.setState({
            showStart: nextStart,
            showStop: nextStop,
        });
        if (this.props.onChange) {
            this.props.onChange(nextStart, nextStop);
        }
    }

    checkIfShow(index) {
        return index >= this.state.showStart && index < this.state.showStop;
    }

    anterior() {
        const nextStart = this.state.showStart - 1;
        const nextStop = this.state.showStop - 1;
        if (nextStart < 0) {
            return;
        }
        this.setState({
            showStart: nextStart,
            showStop: nextStop,
        });
        if (this.props.onChange) {
            this.props.onChange(nextStart, nextStop);
        }
    }

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'row'}}>
                <TouchableHighlight onPress={() => this.anterior()} underlayColor='transparent'>
                    <Icon style={{color: 'white', fontSize: 22}} name="ios-arrow-dropleft"/>
                </TouchableHighlight>
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    {this.checkIfShow(0) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Pontos</Text>}
                    {this.checkIfShow(1) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Placar</Text>}
                    {this.checkIfShow(2) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Progn.</Text>}
                    {this.checkIfShow(3) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Placar Inv</Text>}
                    {this.checkIfShow(4) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Prog. Inv</Text>}
                    {this.checkIfShow(5) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Emp/Vit</Text>}
                    {this.checkIfShow(6) && <Text style={{
                        flex: 1, textAlign: 'center',
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 11
                    }}>Vit/Emp</Text>}
                </View>
                <TouchableHighlight onPress={() => this.proximo()} underlayColor='transparent'>
                    <Icon style={{color: 'white', fontSize: 22}} name="ios-arrow-dropright"/>
                </TouchableHighlight>
            </View>
        );
    }
}


export interface Props {
    navigation: any;
}

export interface State {
}

class ClassificacaoScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            showStart: 0,
            showStop: 3,
            showStartCampeonato: 0,
            showStopCampeonato: 3,
            classificados: 4,
            rebaixados: 4
        };
    }

    componentDidMount() {
        this.props.carregar_classificacao(this.props.bolao.id);
    }

    onChangeShowRodada(start, stop) {
        if (start == null || stop == null) {
            return;
        }
        this.setState({showStart: start, showStop: stop});
    }

    checkIfShow(index) {
        return index >= this.state.showStart && index < this.state.showStop;
    }

    checkIfShowCampeonato(index) {
        return index >= this.state.showStartCampeonato && index < this.state.showStopCampeonato;
    }

    onChangeShowCampeonato(start, stop) {
        if (start == null || stop == null) {
            return;
        }
        this.setState({showStartCampeonato: start, showStopCampeonato: stop});
    }

    renderRankingRodada() {
        const {classificacoesRodada} = this.props;
        let classificacao = null;
        if (classificacoesRodada && classificacoesRodada.classificacao_rodadas) {
            classificacao = classificacoesRodada.classificacao_rodadas.find(it => it.rodada === this.props.rodada.id);
        }
        return classificacao && classificacao.jogadores.map((item, index) => {
            return <View key={index} style={{
                flex: 1,
                flexDirection: 'row',
                padding: 5,
                borderBottomWidth: 1,
                borderColor: '#333d45',
                backgroundColor: index < 6 ? 'green' : index >= classificacao.jogadores.length - 2 ? 'red' : '#283036'
            }}>
                <Text
                    style={{flex: 0.2, textAlign: 'center', color: 'white'}}>{index + 1}</Text>
                <Text style={{flex: 0.8, color: 'white'}}>{item.jogador}</Text>
                <View style={{flex: 1, flexDirection: 'row', marginRight: 5}}>
                    {this.checkIfShow(0) &&
                    <Text style={{flex: 0.95, textAlign: 'center', color: 'white'}}>{item.pontos}</Text>}
                    {this.checkIfShow(1) &&
                    <Text style={{flex: 0.95, textAlign: 'center', color: 'white'}}>{item.pontos_placar_correto}</Text>}
                    {this.checkIfShow(2) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.pontos_prognostico_correto}</Text>}
                    {this.checkIfShow(3) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_placar_invertido}</Text>}
                    {this.checkIfShow(4) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_prognostico_invertido}</Text>}
                    {this.checkIfShow(5) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_empate_deu_vitoria}</Text>}
                    {this.checkIfShow(6) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_vitoria_deu_empate}</Text>}
                </View>
            </View>;
        });
    }

    renderRankingCampeonato() {
        const {classificacoes} = this.props;
        return classificacoes && classificacoes.classificacao_geral.map((item, index) => {
            return <View key={index} style={{
                flex: 1,
                flexDirection: 'row', padding: 5,
                borderBottomWidth: 1, borderColor: '#333d45',
                backgroundColor: index < 6 ? 'green' : index >= classificacoes.classificacao_geral.length - 6 ? 'red' : '#283036'
            }}>
                <Text style={{flex: 0.2, textAlign: 'center', color: 'white'}}>{index + 1}</Text>
                <Text style={{flex: 0.8, color: 'white'}}>{item.jogador}</Text>
                <View style={{flex: 1, flexDirection: 'row', marginRight: 5}}>
                    {this.checkIfShowCampeonato(0) &&
                    <Text style={{flex: 0.95, textAlign: 'center', color: 'white'}}>{item.pontos}</Text>}
                    {this.checkIfShowCampeonato(1) &&
                    <Text style={{flex: 0.95, textAlign: 'center', color: 'white'}}>{item.pontos_placar_correto}</Text>}
                    {this.checkIfShowCampeonato(2) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.pontos_prognostico_correto}</Text>}
                    {this.checkIfShowCampeonato(3) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_placar_invertido}</Text>}
                    {this.checkIfShowCampeonato(4) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_prognostico_invertido}</Text>}
                    {this.checkIfShowCampeonato(5) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_empate_deu_vitoria}</Text>}
                    {this.checkIfShowCampeonato(6) &&
                    <Text style={{
                        flex: 0.95,
                        textAlign: 'center',
                        color: 'white'
                    }}>{item.penalidade_vitoria_deu_empate}</Text>}
                </View>
            </View>;
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <BolaoHeader/>
                <Tabs initialPage={0} style={{flex: 1}}>
                    <Tab heading="Rodada" style={styles.tabContainer}
                         tabStyle={{backgroundColor: '#1b4c72', borderTopWidth: 3, borderColor: '#1a4a70'}}
                         activeTabStyle={{backgroundColor: '#1a4a70'}}
                         textStyle={{fontSize: 12, color: 'white'}}
                         activeTextStyle={{fontSize: 12, color: 'white'}}>
                        <View style={{flex: 1}}>
                            <RodadaSelect/>
                            <Content style={{flex: 1}} refreshControl={
                                <RefreshControl refreshing={this.props.carregandoClassificacaoRodada}
                                                onRefresh={() => this.props.carregar_classificacao_rodada(this.props.bolao.id, this.props.rodada.id)}/>
                            }>
                                {!this.props.carregandoClassificacaoRodada && <View style={styles.table}>
                                    <Text style={{
                                        flex: 0.2,
                                        textAlign: 'center',
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>Pos</Text>
                                    <Text style={{
                                        flex: 0.7,
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>Chutador</Text>
                                    <TableHeaderSelect onChange={this.onChangeShowRodada.bind(this)}/>
                                </View>}
                                {!this.props.carregandoClassificacaoRodada && this.renderRankingRodada()}
                            </Content>
                        </View>
                    </Tab>
                    <Tab heading="Campeonato" style={styles.tabContainer}
                         tabStyle={{backgroundColor: '#1b4c72', borderTopWidth: 3, borderColor: '#1a4a70'}}
                         activeTabStyle={{backgroundColor: '#1a4a70'}}
                         textStyle={{fontSize: 12, color: 'white'}}
                         activeTextStyle={{fontSize: 12, color: 'white'}}>
                        <View style={{flex: 1}}>
                            <Content style={{flex: 1}} refreshControl={
                                <RefreshControl refreshing={this.props.carregandoClassificacao}
                                                onRefresh={() => this.props.carregar_classificacao(this.props.bolao.id)}/>
                            }>
                                {!this.props.carregandoClassificacao && <View style={styles.table}>
                                    <Text style={{
                                        flex: 0.2,
                                        textAlign: 'center',
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>Pos</Text>
                                    <Text style={{
                                        flex: 0.7,
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>Chutador</Text>
                                    <TableHeaderSelect onChange={this.onChangeShowCampeonato.bind(this)}/>
                                </View>}
                                {!this.props.carregandoClassificacao && this.renderRankingCampeonato()}
                            </Content>
                        </View>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({carregar_classificacao, carregar_classificacao_rodada}, dispatch);
const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    rodada: state.homeReducer.rodadaSelecionada,
    carregandoClassificacao: state.homeReducer.carregandoClassificacao,
    carregandoClassificacaoRodada: state.homeReducer.carregandoClassificacaoRodada,
    classificacoes: state.homeReducer.classificacoes,
    classificacoesRodada: state.homeReducer.classificacoesRodada,
});
export default connect(mapStateToProps, mapDispatchToProps)(ClassificacaoScreen);

