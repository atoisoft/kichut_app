import {Alert, AsyncStorage} from 'react-native';
import {genericRequestGet, genericRequestPost} from '../../utils/functions';
import {carregarListaBoloesInscrito} from './auth';
import Http from '../../utils/api';

export const BOLAO_URL = '/boloes/';

export const INSCRICAO_URL = BOLAO_URL + 'inscrever_com_chave/';
export const INSCRICAO_REQUEST = 'INSCRICAO_REQUEST';
export const INSCRICAO_ERROR = 'INSCRICAO_ERROR';
export const INSCRICAO_SUCCESS = 'INSCRICAO_SUCCESS';

export const CARREGAR_RODADA_REQUEST = 'CARREGAR_RODADA_REQUEST';
export const CARREGAR_RODADA_ERROR = 'CARREGAR_RODADA_ERROR';
export const CARREGAR_RODADA_SUCCESS = 'CARREGAR_RODADA_SUCCESS';

export const CARREGAR_PALPITES_REQUEST = 'CARREGAR_PALPITES_REQUEST';
export const CARREGAR_PALPITES_ERROR = 'CARREGAR_PALPITES_ERROR';
export const CARREGAR_PALPITES_SUCCESS = 'CARREGAR_PALPITES_SUCCESS';

export const CARREGAR_PARTIDA_REQUEST = 'CARREGAR_PARTIDA_REQUEST';
export const CARREGAR_PARTIDA_ERROR = 'CARREGAR_PARTIDA_ERROR';
export const CARREGAR_PARTIDA_SUCCESS = 'CARREGAR_PARTIDA_SUCCESS';

export const PALPITE_REQUEST = 'PALPITE_REQUEST';
export const PALPITE_ERROR = 'PALPITE_ERROR';
export const PALPITE_SUCCESS = 'PALPITE_SUCCESS';

export const CARREGAR_BOLAO_REQUEST = 'CARREGAR_BOLAO_REQUEST';
export const CARREGAR_BOLAO_ERROR = 'CARREGAR_BOLAO_ERROR';
export const CARREGAR_BOLAO_SUCCESS = 'CARREGAR_BOLAO_SUCCESS';

export const CARREGAR_CLASSIFICACAO_REQUEST = 'CARREGAR_CLASSIFICACAO_REQUEST';
export const CARREGAR_CLASSIFICACAO_ERROR = 'CARREGAR_CLASSIFICACAO_ERROR';
export const CARREGAR_CLASSIFICACAO_SUCCESS = 'CARREGAR_CLASSIFICACAO_SUCCESS';

export const CARREGAR_CLASSIFICACAO_RODADA_REQUEST = 'CARREGAR_CLASSIFICACAO_RODADA_REQUEST';
export const CARREGAR_CLASSIFICACAO_RODADA_ERROR = 'CARREGAR_CLASSIFICACAO_RODADA_ERROR';
export const CARREGAR_CLASSIFICACAO_RODADA_SUCCESS = 'CARREGAR_CLASSIFICACAO_RODADA_SUCCESS';

export function inscricao_com_chave(chave) {
    return genericRequestPost(INSCRICAO_URL, {chave_acesso: chave}, INSCRICAO_REQUEST, INSCRICAO_SUCCESS, INSCRICAO_ERROR, (success, dispatch) => {
        Alert.alert('Parabéns', 'Inscrição efetuada com sucesso');
        dispatch(carregarListaBoloesInscrito());
    });
}

export function cadastrar_palpite(bolao_id, data) {
    const URL = BOLAO_URL + bolao_id + '/cadastrar_palpite/';
    return genericRequestPost(URL, {
        partida: data.partida,
        gols_equipe_casa: data.gols_equipe_casa,
        gols_equipe_fora: data.gols_equipe_fora,
        quem_passa_mata_mata: data.quem_passa_mata_mata
    }, PALPITE_REQUEST, PALPITE_SUCCESS, PALPITE_ERROR);
}

export function carregar_rodada(bolao_id, rodada_id) {
    const URL = BOLAO_URL + bolao_id + '/carregar_rodada/?rodada=' + rodada_id;
    return genericRequestGet(URL, CARREGAR_RODADA_REQUEST, CARREGAR_RODADA_SUCCESS, CARREGAR_RODADA_ERROR, (result, dispatch) => {
        dispatch(carregar_palpites(bolao_id, rodada_id));
        dispatch(carregar_classificacao_rodada(bolao_id, rodada_id));
    });
}

export function carregar_palpites(bolao_id, rodada_id) {
    const URL = BOLAO_URL + bolao_id + '/meus_palpites/?rodada=' + rodada_id;
    return genericRequestGet(URL, CARREGAR_PALPITES_REQUEST, CARREGAR_PALPITES_SUCCESS, CARREGAR_PALPITES_ERROR);
}

export function carregar_bolao(bolao_id) {
    const URL = BOLAO_URL + bolao_id + '/detalhado/?app=1';
    return genericRequestGet(URL, CARREGAR_BOLAO_REQUEST, CARREGAR_BOLAO_SUCCESS, CARREGAR_BOLAO_ERROR, (result, dispatch) => {
        dispatch(carregar_rodada(result.id, result.campeonato.rodada_atual.toString()));
        AsyncStorage.setItem('@Bolao:selecionado_id', result.id.toString());
    });
}

export function carregar_classificacao_rodada(bolao_id, rodada_id) {
    const URL = BOLAO_URL + bolao_id + '/classificacao_rodada/?rodada=' + rodada_id;
    return genericRequestGet(URL, CARREGAR_CLASSIFICACAO_RODADA_REQUEST, CARREGAR_CLASSIFICACAO_RODADA_SUCCESS, CARREGAR_CLASSIFICACAO_RODADA_ERROR);
}

export function carregar_classificacao(bolao_id) {
    const URL = BOLAO_URL + bolao_id + '/classificacao_geral/';
    return genericRequestGet(URL, CARREGAR_CLASSIFICACAO_REQUEST, CARREGAR_CLASSIFICACAO_SUCCESS, CARREGAR_CLASSIFICACAO_ERROR);
}

export function carregar_partida(partida) {
    const URL = '/partidas/' + partida + '/detalhada/';
    return (dispatch, getState) => {
        const {bolao} = getState().homeReducer;
        dispatch({type: CARREGAR_PARTIDA_REQUEST, data: {bolao: bolao.id}});
        Http.post(URL, {bolao: bolao.id}, (result) => {
            dispatch({
                type: CARREGAR_PARTIDA_SUCCESS,
                payload: result
            });
        }, (result) => {
            dispatch({
                type: CARREGAR_PARTIDA_ERROR,
                payload: result
            });
        });
    };
}
