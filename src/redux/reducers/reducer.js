import {
    CARREGAR_BOLAO_REQUEST,
    CARREGAR_BOLAO_SUCCESS,
    CARREGAR_CLASSIFICACAO_ERROR,
    CARREGAR_CLASSIFICACAO_REQUEST, CARREGAR_CLASSIFICACAO_RODADA_ERROR, CARREGAR_CLASSIFICACAO_RODADA_REQUEST, CARREGAR_CLASSIFICACAO_RODADA_SUCCESS,
    CARREGAR_CLASSIFICACAO_SUCCESS,
    CARREGAR_PALPITES_ERROR, CARREGAR_PALPITES_REQUEST,
    CARREGAR_PALPITES_SUCCESS, CARREGAR_PARTIDA_ERROR, CARREGAR_PARTIDA_REQUEST, CARREGAR_PARTIDA_SUCCESS, CARREGAR_RODADA_REQUEST, CARREGAR_RODADA_SUCCESS,
    PALPITE_ERROR,
    PALPITE_SUCCESS
} from '../actions/common';
import {Alert} from 'react-native';
import {LOGOUT} from '../actions/auth';
import * as utils from '../../utils/functions';
import * as ACTION from "../../screens/HomeScreen/actions";

const initialState = {
    bolao: null,
    rodadaSelecionada: null,
    partidaSelecionada: null,
    rodadaSelecionadaIndex: 0,
    partidaSelecionadaIndex: 0,
    partidaGrupoSelecionadaIndex: 0,
    meus_palpites: null,
    carregandoBolao: false,
    carregandoRodada: false,
    carregandoClassificacao: false,
    carregandoClassificacaoRodada: false,
    classificacoes: null,
    classificacoesRodada: null,
    carregandoPartida: false,
    partidas: null,
    partidas_agrupadas: null
};

export default function (state: any = initialState, action: Function) {
    switch (action.type) {
        case LOGOUT:
            return initialState;
        case CARREGAR_BOLAO_REQUEST:
            return Object.assign({}, state, {
                carregandoBolao: true,
            });
        case CARREGAR_BOLAO_SUCCESS:
            //Carregando a próxima rodada após a encerrada
            let encontrado = false;
            let idx_inicial = 0;
            action.payload.campeonato.rodadas.forEach(it => {
                if (!encontrado) {
                    if (!it.encerrada) {
                        encontrado = true;
                    }
                    if (!encontrado) {
                        idx_inicial += 1;
                    }
                }
            });

            return {
                ...state,
                bolao: action.payload,
                rodadaSelecionada: null,
                rodadaSelecionadaIndex: idx_inicial,
                partidaSelecionadaIndex: 0,
                partidaGrupoSelecionadaIndex: 0,
                partidaSelecionada: null,
                carregandoBolao: false,
                partidas: null,
                partidas_agrupadas: null
            };
        case CARREGAR_RODADA_REQUEST:
            return Object.assign({}, state, {
                carregandoRodada: true,
                carregandoClassificacaoRodada: true,
            });
        case CARREGAR_RODADA_SUCCESS:
            const result = [];
            action.payload.partidas.forEach(it => {
                let grupo_ja_inserido = null;
                result.forEach(gp => {
                    if (gp.data === utils.data(it.datahora_partida)) {
                        grupo_ja_inserido = gp;
                    }
                });
                if (grupo_ja_inserido) {
                    grupo_ja_inserido.partidas.push(it);
                } else {
                    let grupo_item = {data: utils.data(it.datahora_partida), partidas: [it,]};
                    result.push(grupo_item);
                }
            });
            return {
                ...state,
                carregandoRodada: true,
                carregandoBolao: false,
                rodadaSelecionada: action.payload,
                partidas: action.payload.partidas,
                partidas_agrupadas: result.length > 0 ? result : null
            };
        case CARREGAR_PALPITES_REQUEST:
            return Object.assign({}, state, {
                meus_palpites: null,
                carregandoRodada: true,
            });
        case CARREGAR_PALPITES_SUCCESS:
            return Object.assign({}, state, {
                meus_palpites: action.payload,
                carregandoRodada: false,
            });
        case CARREGAR_PALPITES_ERROR:
            Alert.alert('Atenção', 'Não foi possível carregar os palpites');
            return Object.assign({}, state, {
                meus_palpites: null,
                carregandoRodada: false,
            });
        case PALPITE_SUCCESS:
            return Object.assign({}, state, {
                meus_palpites: action.payload
            });
        case CARREGAR_PARTIDA_REQUEST:
            return Object.assign({}, state, {
                carregandoPartida: true
            });
        case CARREGAR_PARTIDA_SUCCESS:
            return Object.assign({}, state, {
                carregandoPartida: false,
                partidaSelecionada: action.payload
            });
        case CARREGAR_PARTIDA_ERROR:
            return Object.assign({}, state, {
                carregandoPartida: false
            });
        case PALPITE_ERROR:
            return state;
        case CARREGAR_CLASSIFICACAO_REQUEST:
            return Object.assign({}, state, {
                carregandoClassificacao: true
            });
        case CARREGAR_CLASSIFICACAO_RODADA_REQUEST:
            return Object.assign({}, state, {
                carregandoClassificacaoRodada: true
            });
        case CARREGAR_CLASSIFICACAO_SUCCESS:
            return Object.assign({}, state, {
                classificacoes: action.payload,
                carregandoClassificacao: false
            });
        case CARREGAR_CLASSIFICACAO_RODADA_SUCCESS:
            return Object.assign({}, state, {
                classificacoesRodada: action.payload,
                carregandoClassificacaoRodada: false
            });
        case CARREGAR_CLASSIFICACAO_ERROR:
            return Object.assign({}, state, {
                carregandoClassificacao: false
            });
        case CARREGAR_CLASSIFICACAO_RODADA_ERROR:
            return Object.assign({}, state, {
                carregandoClassificacaoRodada: false
            });
        case ACTION.RODADA_ANTERIOR:
            return Object.assign({}, state, {
                rodadaSelecionadaIndex: action.payload
            });
        case ACTION.RODADA_PROXIMA:
            return Object.assign({}, state, {
                rodadaSelecionadaIndex: action.payload
            });
        case ACTION.SELECIONAR_PARTIDA:
            return Object.assign({}, state, {
                carregandoPartida: true,
                partidaSelecionada: action.payload.partida,
                partidaSelecionadaIndex: action.payload.index_partida,
                partidaGrupoSelecionadaIndex: action.payload.index_grupo
            });
        case ACTION.PARTIDA_PROXIMA:
            let newIndex = state.partidaSelecionadaIndex + 1;
            let partidaSelecionadaNova = null;
            let partidaSelecionadaIndex = state.partidaSelecionadaIndex;
            state.carregandoPartida = true;
            //Verificar se tem partida no grupo
            const grupo = state.partidas_agrupadas[state.partidaGrupoSelecionadaIndex];
            if (grupo != null) {
                let partida = grupo.partidas[newIndex];
                if (partida != null) {
                    partidaSelecionadaNova = partida;
                    partidaSelecionadaIndex = newIndex;
                } else {//Não tem mais partida no grupo selecionado
                    const newIndexGrupo = state.partidaGrupoSelecionadaIndex + 1;

                    let grupoEncontrado = state.partidas_agrupadas[newIndexGrupo];
                    if (grupoEncontrado == null) { //Não tem mais grupos
                        return state;
                    } else {
                        return Object.assign({}, state, {
                            partidaSelecionada: grupoEncontrado.partidas[0],
                            partidaSelecionadaIndex: 0,
                            partidaGrupoSelecionadaIndex: newIndexGrupo
                        });
                    }
                }
            }
            state.carregandoPartida = false;
            return Object.assign({}, state, {
                partidaSelecionada: partidaSelecionadaNova,
                partidaSelecionadaIndex: partidaSelecionadaIndex
            });
        case ACTION.PARTIDA_ANTERIOR:
            newIndex = state.partidaSelecionadaIndex - 1;
            partidaSelecionadaNova = null;
            partidaSelecionadaIndex = state.partidaSelecionadaIndex;

            //Verificar se tem partida no grupo
            let grupoNew = state.partidas_agrupadas[state.partidaGrupoSelecionadaIndex];
            if (grupoNew != null) {
                let partida = grupoNew.partidas[newIndex];
                if (partida != null) {
                    partidaSelecionadaNova = partida;
                    partidaSelecionadaIndex = newIndex;
                } else {//Não tem mais partida no grupo selecionado
                    const newIndexGrupo = state.partidaGrupoSelecionadaIndex - 1;
                    let grupoEncontrado = state.partidas_agrupadas[newIndexGrupo];

                    if (grupoEncontrado == null) { //Não tem mais grupos
                        return state;
                    } else {
                        return Object.assign({}, state, {
                            partidaSelecionada: grupoEncontrado.partidas[grupoEncontrado.partidas.length - 1],
                            partidaSelecionadaIndex: grupoEncontrado.partidas.length - 1,
                            partidaGrupoSelecionadaIndex: newIndexGrupo
                        });
                    }
                }
            }

            return Object.assign({}, state, {
                partidaSelecionada: partidaSelecionadaNova,
                partidaSelecionadaIndex: partidaSelecionadaIndex
            });
        default:
            return state;
    }
}
