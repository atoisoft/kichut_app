import * as React from 'react';
import {connect} from 'react-redux';
import {
    Text,
    Tab,
    Tabs,
    Button, Toast
} from 'native-base';
import {View, Dimensions, Alert, AsyncStorage, RefreshControl, TextInput} from 'react-native';
import {Container, Content} from 'native-base';
import styles from './styles';
import {atualizar_usuario, carregarListaBoloesInscrito, logout} from '../../redux/actions/auth';
import {inscricao_com_chave} from '../../redux/actions/common';
import {isIphoneX} from "../../utils/functions";

const width = Dimensions.get('window').width;

export interface Props {
    navigation: any;
}

export interface State {
}

class ConfiguracaoScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            date: null,
            chave: '',
            username: '',
            email: ''
        };
    }

    componentDidMount(): void {
        this.setState({username: this.props.usuario.username, email: this.props.usuario.email});
    }

    onClickInscrever() {
        this.props.inscricao_com_chave(this.state.chave);
    }

    salvar() {
        // Toast.show({
        //     text: 'Preencha os campos corretamente',
        //     buttonText: 'OK'
        // });
        this.props.atualizar_usuario({username: this.state.username, email: this.state.email});
    }

    logout() {
        AsyncStorage.removeItem('@Login:id');
        AsyncStorage.removeItem('@Login:token');
        AsyncStorage.removeItem('@Bolao:selecionado_id');
        this.props.logout();
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <Container style={styles.container}>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#1b4c72'
                }}>
                    <View style={{justifyContent: 'center', alignItems: 'center', padding: 10, height: 65}}>
                        <Text style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            color: 'white'
                        }}>CONFIGURAÇÕES</Text>
                    </View>
                </View>
                <Tabs initialPage={0} keyboardShouldPersistTaps="handled">
                    <Tab heading="Meus dados"
                         style={{backgroundColor: '#283036'}}
                         tabStyle={{backgroundColor: '#1b4c72', borderTopWidth: 3, borderColor: '#1a4a70'}}
                         activeTabStyle={{backgroundColor: '#1a4a70'}}
                         textStyle={{fontSize: 12, color: 'white'}}
                         activeTextStyle={{fontSize: 12, color: 'white'}}>
                        <Content style={{padding: 10}}>
                            <View style={{flex: 1}}>
                                <TextInput style={styles.input} autoCapitalize="none"
                                           ref={(input) => this.usernameInput = input}
                                           onSubmitEditing={() => this.emailInput.focus()}
                                           onChangeText={(username) => this.setState({username})}
                                           value={this.state.username}
                                           autoCorrect={false}
                                           keyboardType='default'
                                           returnKeyType="next"
                                           placeholder='Apelido'
                                           placeholderTextColor='rgba(225,225,225,0.7)'/>
                                <TextInput style={styles.input} autoCapitalize="none"
                                           ref={(input) => this.emailInput = input}
                                           onChangeText={(email) => this.setState({email})}
                                           value={this.state.email}
                                           autoCorrect={false}
                                           keyboardType='email-address'
                                           returnKeyType="next"
                                           placeholder='E-mail'
                                           placeholderTextColor='rgba(225,225,225,0.7)'/>
                            </View>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <Button block bordered style={{
                                    backgroundColor: '#009538',
                                    borderColor: 'white',
                                }} onPress={() => this.salvar()}><Text style={{color: 'white'}}>SALVAR</Text></Button>
                                <Button block bordered style={{
                                    marginTop: 20,
                                    backgroundColor: '#5a5d5f',
                                    borderColor: 'white',
                                }} onPress={() => this.logout()}><Text style={{color: 'white'}}>SAIR</Text></Button>
                            </View>
                        </Content>
                    </Tab>
                    <Tab heading="Meus bolões"
                         keyboardShouldPersistTaps="handled"
                         style={{backgroundColor: '#283036',}}
                         tabStyle={{backgroundColor: '#1b4c72', borderTopWidth: 3, borderColor: '#1a4a70'}}
                         activeTabStyle={{backgroundColor: '#1a4a70'}}
                         textStyle={{fontSize: 12, color: 'white'}}
                         activeTextStyle={{fontSize: 12, color: 'white'}}>
                        <View style={{flex: isIphoneX() ? 0.4 : 0.6, justifyContent: 'center'}}
                              padder>
                            <View style={{
                                flex: 0.8,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                padding: 10
                            }}>
                                <View style={{flex: 1}}>
                                    <TextInput style={styles.input} autoCapitalize="none"
                                               ref={(input) => this.chaveInput = input}
                                               onChangeText={(chave) => this.setState({chave})}
                                               value={this.state.chave}
                                               autoCorrect={false}
                                               keyboardType='default'
                                               placeholder='Digite aqui seu código de acesso'
                                               placeholderTextColor='rgba(225,225,225,0.7)'/>
                                </View>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                }}>
                                    <Button block bordered
                                            style={{
                                                backgroundColor: '#009538',
                                                borderColor: 'white',
                                            }}>
                                        <Text style={{color: 'white', textAlign: 'center'}}
                                              onPress={() => this.onClickInscrever()}>ENTRAR</Text></Button>
                                </View>
                            </View>
                        </View>
                        <Content style={{flex: 1}} refreshControl={
                            <RefreshControl refreshing={this.props.carregandoBoloes}
                                            onRefresh={() => this.props.carregarListaBoloesInscrito()}/>
                        }>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#2e373e',
                                alignItems: 'center',
                                height: 40
                            }}>
                                <View style={{flex: 1, alignItems: 'center'}}>
                                    <Text style={{color: 'white', fontWeight: 'bold'}}>Bolão</Text>
                                </View>
                                <View style={{flex: 1, alignItems: 'center'}}>
                                    <Text style={{color: 'white', fontWeight: 'bold'}}>Campeonato</Text>
                                </View>
                                <View style={{flex: 1, alignItems: 'center'}}>
                                    <Text style={{color: 'white', fontWeight: 'bold'}}>Status</Text>
                                </View>
                            </View>
                            {!this.props.carregandoBoloes && this.props.boloes_inscrito &&
                            <View style={{flex: 1, alignItems: 'center'}}>
                                {this.props.boloes_inscrito.map((item, index) => {
                                    return (
                                        <View key={index} style={{
                                            flexDirection: 'row',
                                            padding: 10, borderBottomWidth: 0.8, borderColor: '#333d45',
                                        }}>
                                            <View style={{flex: 1, paddingLeft: 5}}>
                                                <Text style={{color: 'white'}}>{item.bolao.nome}</Text>
                                            </View>
                                            <View style={{flex: 1, alignItems: 'center'}}>
                                                <Text style={{color: 'white'}}>{item.bolao.campeonato.nome}</Text>
                                            </View>
                                            <View style={{flex: 1, alignItems: 'center'}}>
                                                <Text style={{color: 'white'}}>{item.status_inscricao_display}</Text>
                                            </View>
                                        </View>
                                    );
                                })}
                            </View>}
                        </Content>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

function bindAction(dispatch) {
    return {
        inscricao_com_chave: chave => dispatch(inscricao_com_chave(chave)),
        atualizar_usuario: dados => dispatch(atualizar_usuario(dados)),
        carregarListaBoloesInscrito: () => dispatch(carregarListaBoloesInscrito()),
        logout: () => dispatch(logout())
    };
}

const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    usuario: state.authReducer.usuario,
    boloes_inscrito: state.authReducer.boloes_inscrito,
    carregandoBoloes: state.authReducer.carregandoBoloes
});
export default connect(mapStateToProps, bindAction)(ConfiguracaoScreen);
