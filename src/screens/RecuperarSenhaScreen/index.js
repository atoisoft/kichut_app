import * as React from 'react';
import {connect} from "react-redux";
import {
    Container,
    Content, Button, Text, View, Toast
} from 'native-base';
import styles from "../CadastroScreen/styles";
import {Dimensions, Image, TextInput, ActivityIndicator} from "react-native";
import {bindActionCreators} from "redux";
import {recuperar_senha} from "../../redux/actions/auth";

const {width} = Dimensions.get('window');
const HeaderLogo = require('../../assets/images/header_logo.png');

export interface Props {
    navigation: any;
}

export interface State {
}

class RecuperarSenhaScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.alterandoSenhaSucesso && !this.props.inicializando) {
            this.props.navigation.navigate('LoaderScreen');
        }
    }

    handleRecuperar() {
        if (this.props.alterandoSenha) {
            Toast.show({
                text: 'Aguarde um instante...',
                buttonText: 'OK'
            });
            return;
        }
        if (!this.state.email || this.state.email == '') {
            Toast.show({
                text: 'Preencha os campos corretamente',
                buttonText: 'OK'
            });
            return;
        }
        this.props.recuperar_senha({email: this.state.email});
    }

    handleCancelar() {
        this.props.navigation.navigate('LoaderScreen');
    }

    render() {
        return (
            <Container style={styles.container}>
                <View>
                    <Image source={HeaderLogo}
                           style={{
                               padding: 0,
                               margin: 0,
                               resizeMode: 'contain',
                               width: width,
                               height: 55,
                               top: 0
                           }}/>
                </View>
                {this.props.alterandoSenha || this.props.inicializando ?
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color="#ffffff"/>
                        <Text style={{color: 'white', textAlign: 'center'}}>Aguarde...</Text>
                    </View> :
                    <Content style={{padding: 30}}>
                        <View style={{flex: 0.1, marginBottom: 25}}>
                            <Text style={{textAlign: 'center', color: 'white', fontSize: 25}}>Recuperar senha</Text>
                        </View>
                        <View style={{flex: 0.1, marginBottom: 25}}>
                            <Text style={{textAlign: 'left', color: 'white', fontSize: 12}}>Informe seu e-mail, clique
                                em recuperar e confira o seu e-mail para alterar a senha</Text>
                        </View>
                        <View style={{flex: 0.7}}>
                            <TextInput style={styles.input} autoCapitalize="none"
                                       ref={(input) => this.emailInput = input}
                                       onChangeText={(email) => this.setState({email})}
                                       value={this.state.email}
                                       autoCorrect={false}
                                       keyboardType='email-address'
                                       returnKeyType="next"
                                       placeholder='E-mail'
                                       placeholderTextColor='rgba(225,225,225,0.7)'/>
                        </View>
                        <View style={{flex: 0.2, flexDirection: 'row', justifyContent: 'center', paddingTop: 10}}>
                            <Button bordered block style={{
                                flex: 0.8,
                                marginRight: 10,
                                borderColor: 'white',
                                borderRadius: 8,
                                height: 50
                            }} onPress={this.handleCancelar.bind(this)}><Text
                                style={{color: 'white', textAlign: 'center'}}>CANCELAR</Text></Button>
                            <Button bordered block style={{
                                flex: 1.2,
                                backgroundColor: '#009538',
                                borderColor: 'white', height: 50, borderRadius: 8,
                            }} onPress={this.handleRecuperar.bind(this)}><Text
                                style={{color: 'white', textAlign: 'center'}}>RECUPERAR</Text></Button>
                        </View>
                    </Content>}
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({recuperar_senha}, dispatch);
const mapStateToProps = state => ({
    usuario: state.authReducer.usuario,
    alterandoSenha: state.authReducer.alterandoSenha,
    alterandoSenhaSucesso: state.authReducer.alterandoSenhaSucesso,
    inicializando: state.authReducer.inicializando
});
export default connect(mapStateToProps, mapDispatchToProps)(RecuperarSenhaScreen);
