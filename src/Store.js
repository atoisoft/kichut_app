import {Platform} from 'react-native';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import devTools from 'remote-redux-devtools';

import reducer from './redux/reducers';

export default function configureStore(onCompletion: () => void): any {
    const enhancer = compose(
        applyMiddleware(thunk),
        devTools({
            name: Platform.OS,
            realtime: true
        })
    );
    return createStore(reducer, enhancer);
}
