import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Container} from "native-base";
import {ActivityIndicator, StyleSheet, AsyncStorage} from 'react-native';
import {ler_usuario} from "../redux/actions/auth";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

class LoadingScreen extends React.Component {
    static navigationOptions = {};

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('@Login:token');
        if (userToken) {
            this.props.ler_usuario(userToken);
        } else {
            this.props.navigation.navigate('Auth');
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.usuario != null && !nextProps.autenticando && nextProps.bolao) {
            this.props.navigation.navigate('App');
        }
    }

    render() {
        return (
            <Container style={styles.container}>
                <ActivityIndicator size="large" color="#ffffff"/>
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ler_usuario}, dispatch);

const mapStateToProps = state => ({
    autenticando: state.authReducer.autenticando,
    bolao: state.homeReducer.bolao,
    usuario: state.authReducer.usuario,
});

export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen);
