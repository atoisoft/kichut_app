import * as React from 'react';
import {connect} from 'react-redux';
import {View, Fab, Icon, Button, Content, Toast} from 'native-base';
import {Text, Modal, TouchableHighlight, Alert, RefreshControl} from 'react-native';
import RodadaSelect from './RodadaSelect';
import {carregar_bolao} from '../redux/actions/common';
import {carregarListaBoloesInscrito} from '../redux/actions/auth';
import {bindActionCreators} from "redux";

export interface Props {
    showRodada: boolean;
}

export interface State {
}

class BolaoHeader extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false
        };
    }

    changeBolao() {
        this.props.carregarListaBoloesInscrito();
        this.setModalVisible(true);
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    selecionarBolao(bolao_inscricao) {
        if (bolao_inscricao.bolao.id === this.props.bolao.id) {
            this.setModalVisible(false);
            return;
        }
        if (bolao_inscricao.status_inscricao === 'AGUARDANDO') {
            Alert.alert('Atenção', 'Inscrição ainda não confirmada');
            return;
        }
        this.props.carregar_bolao(bolao_inscricao.bolao.id);
        this.setModalVisible(false);
    }

    render() {
        return this.props.bolao && (
            <View style={{
                alignItems: 'center',
                backgroundColor: '#1b4c72',
                flexDirection: 'row'
            }}>
                <View style={{alignItems: 'center', padding: 10, flex: 1, height: 68, justifyContent: 'center'}}>
                    <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white', marginTop: 7}}>{this.props.bolao.nome}</Text>
                    <Text style={{
                        fontSize: 13,
                        fontWeight: 'bold',
                        color: 'white', marginTop: 7
                    }}>{this.props.bolao.campeonato.nome}</Text>
                    <Fab
                        active={true}
                        containerStyle={{top: 0, right: 0, bottom: 0}}
                        style={{backgroundColor: '#009538', flex: 1, justifyContent: 'center', width: 35, height: 35}}
                        position="topRight"
                        onPress={() => this.changeBolao()}>
                        <Icon name="ios-add"/>
                    </Fab>
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(false);
                    }}>
                    <Content style={{backgroundColor: '#283036'}} refreshControl={
                        <RefreshControl refreshing={this.props.carregandoBoloes}
                                        onRefresh={() => this.props.carregarListaBoloesInscrito()}/>
                    }>
                        <View style={{borderBottomWidth: 1, borderColor: '#333d45'}}>
                            <Text style={{
                                color: 'white',
                                marginBottom: 10,
                                marginTop: 10,
                                fontSize: 18,
                                textAlign: 'center'
                            }}>Selecionar
                                bolão</Text>
                        </View>
                        {!this.props.carregandoBoloes && this.props.boloes_inscrito &&
                        <View style={{flex: 1, alignItems: 'center'}}>
                            {this.props.boloes_inscrito.map((item, index) => {
                                return (
                                    <TouchableHighlight key={index} style={{
                                        flexDirection: 'row',
                                        padding: 10, borderBottomWidth: 0.8, borderColor: '#333d45',
                                    }} onPress={() => this.selecionarBolao(item)}>
                                        <View style={{flex: 1, flexDirection: 'row'}}>
                                            <View style={{flex: 1, paddingLeft: 5}}>
                                                <Text style={{color: 'white'}}>{item.bolao.nome}</Text>
                                            </View>
                                            <View style={{flex: 1, alignItems: 'center'}}>
                                                <Text style={{color: 'white'}}>{item.bolao.campeonato.nome}</Text>
                                            </View>
                                            <View style={{flex: 1, alignItems: 'center'}}>
                                                <Text style={{color: 'white'}}>{item.status_inscricao_display}</Text>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                );
                            })}
                        </View>}
                    </Content>
                </Modal>
                {this.props.showRodada && <RodadaSelect/>}
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({carregar_bolao, carregarListaBoloesInscrito}, dispatch);

const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    rodadaSelecionada: state.homeReducer.rodadaSelecionada,
    usuario: state.authReducer.usuario,
    carregandoBoloes: state.authReducer.carregandoBoloes,
    boloes_inscrito: state.authReducer.boloes_inscrito
});
export default connect(mapStateToProps, mapDispatchToProps)(BolaoHeader);
