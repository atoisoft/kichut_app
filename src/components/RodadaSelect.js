import * as React from 'react';
import {connect} from 'react-redux';
import {Icon, View} from 'native-base';
import {Text, TouchableHighlight} from 'react-native';
import {rodadaAnterior, rodadaProxima} from '../screens/HomeScreen/actions';
import {bindActionCreators} from "redux";

export interface Props {
    backgroundColor: string;
}

export interface State {
}

class RodadaSelect extends React.Component<Props, State> {

    existeRodadaAnterior() {
        if (this.props.rodadaSelecionadaIndex === 0) {
            return false;
        }
        return true;
    }

    existeRodadaProxima() {
        if (this.props.rodadaSelecionadaIndex === this.props.bolao.campeonato.rodadas.length - 1) {
            return false;
        }
        return true;
    }

    proxima() {
        if (this.props.rodadaProxima && !this.props.carregandoRodada) {
            if (this.props.rodadaSelecionadaIndex === this.props.bolao.campeonato.rodadas.length - 1) {
                this.props.rodadaProxima(this.props.bolao.id, this.props.rodadaSelecionada.id, this.props.rodadaSelecionadaIndex);
            } else {
                this.props.rodadaProxima(this.props.bolao.id, this.props.bolao.campeonato.rodadas[this.props.rodadaSelecionadaIndex + 1].id, this.props.rodadaSelecionadaIndex + 1);
            }
        }
    }

    anterior() {
        if (this.props.rodadaAnterior && !this.props.carregandoRodada) {
            if (this.props.rodadaSelecionadaIndex === 0) {
                this.props.rodadaAnterior(this.props.bolao.id, this.props.rodadaSelecionada.id, this.props.rodadaSelecionadaIndex);
            } else {
                this.props.rodadaAnterior(this.props.bolao.id, this.props.bolao.campeonato.rodadas[this.props.rodadaSelecionadaIndex - 1].id, this.props.rodadaSelecionadaIndex - 1);
            }
        }
    }

    render() {
        const {rodadaSelecionada} = this.props;
        return rodadaSelecionada && (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                padding: 10,
                height: 50,
                backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : 'transparent'
            }}>
                <TouchableHighlight onPress={() => this.anterior()} underlayColor='transparent'>
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingRight: 20,
                        paddingLeft: 20
                    }}>
                        {this.existeRodadaAnterior() && <Icon style={{color: 'white'}} name="ios-arrow-dropleft"/>}
                    </View>
                </TouchableHighlight>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Text style={{color: 'white'}}>Rodada Nº </Text><Text
                    style={{fontWeight: 'bold', color: 'white'}}>{rodadaSelecionada.numero || 0}</Text>
                </View>
                <TouchableHighlight onPress={() => this.proxima()} underlayColor='transparent'>
                    <View style={{
                        flex: 1,
                        paddingLeft: 20,
                        paddingRight: 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        {this.existeRodadaProxima() && <Icon style={{color: 'white'}} name="ios-arrow-dropright"/>}
                    </View>
                </TouchableHighlight>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({rodadaProxima, rodadaAnterior}, dispatch);
const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    rodadaSelecionada: state.homeReducer.rodadaSelecionada,
    rodadaSelecionadaIndex: state.homeReducer.rodadaSelecionadaIndex,
    isLoading: state.homeReducer.isLoading,
    carregandoRodada: state.homeReducer.carregandoRodada,
});
export default connect(mapStateToProps, mapDispatchToProps)(RodadaSelect);
