import {StyleSheet} from 'react-native';

const styles: any = StyleSheet.create({
    container: {
        backgroundColor: '#283036',
    },
    input: {
        borderWidth: 1,
        borderColor: '#fff',
        padding: 5,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 10,
        color: '#fff'
    }
});
export default styles;
