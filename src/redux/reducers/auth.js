import {
    LOGIN_SUCCESS,
    LOGOUT,
    LOGIN_ERROR,
    LOGIN_REQUEST,
    CADASTRAR_SUCCESS,
    CADASTRAR_ERROR,
    CADASTRAR_REQUEST,
    ATUALIZAR_SUCCESS,
    CARREGAR_SUCCESS,
    CARREGAR_REQUEST,
    CARREGAR_USUARIO_SUCCESS,
    CADASTRAR_USUARIO_REQUEST,
    CADASTRAR_USUARIO_SUCCESS,
    CARREGAR_LISTA_BOLOES_INSCRITO_SUCCESS,
    CARREGAR_LISTA_BOLOES_INSCRITO_REQUEST,
    CARREGAR_LISTA_BOLOES_INSCRITO_ERROR,
    CADASTRAR_USUARIO_ERROR,
    RECUPERAR_SENHA_REQUEST, RECUPERAR_SENHA_SUCCESS, RECUPERAR_SENHA_ERROR
} from '../actions/auth';
import Http from '../../utils/api';
import {CARREGAR_BOLAO_ERROR, CARREGAR_BOLAO_REQUEST, CARREGAR_BOLAO_SUCCESS} from '../actions/common';
import {AsyncStorage} from "react-native";

const initialState = {
    usuario: null,
    autenticando: false,
    inicializando: false,
    carregandoBoloes: false,
    boloes_inscrito: null,
    alterandoSenha: false,
    alterandoSenhaSucesso: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return Object.assign({}, state, {
                usuario: null,
                autenticando: true,
                inicializando: true
            });
        case LOGIN_SUCCESS:
        case CADASTRAR_USUARIO_SUCCESS:
            const user = action.payload.user;
            Http.setAxiosAuthorization(user.token);
            AsyncStorage.setItem('@Login:token', user.token);
            //AsyncStorage.setItem('@Login:user', JSON.stringify(user));
            return Object.assign({}, state, {
                usuario: action.payload.user,
                autenticando: false,
                boloes_inscrito: action.payload.user.boloes_inscrito
            });
        case CARREGAR_USUARIO_SUCCESS:
            return Object.assign({}, state, {
                usuario: action.payload,
                autenticando: false,
                boloes_inscrito: action.payload.boloes_inscrito
            });
        case LOGIN_ERROR:
        case CADASTRAR_USUARIO_ERROR:
            return Object.assign({}, state, {
                erro: action.payload,
                autenticando: false,
                inicializando: false,
            });
        case RECUPERAR_SENHA_REQUEST:
            return Object.assign({}, state, {
                erro: action.payload,
                alterandoSenha: true,
            });
        case RECUPERAR_SENHA_SUCCESS:
            return Object.assign({}, state, {
                erro: action.payload,
                alterandoSenha: false,
                alterandoSenhaSucesso: true
            });
        case RECUPERAR_SENHA_ERROR:
            return Object.assign({}, state, {
                erro: action.payload,
                alterandoSenha: false
            });
        case CARREGAR_BOLAO_REQUEST:
            return Object.assign({}, state, {inicializando: true});
        case CARREGAR_BOLAO_SUCCESS:
            return Object.assign({}, state, {inicializando: false});
        case CARREGAR_BOLAO_ERROR:
            return Object.assign({}, state, {inicializando: false});
        case CADASTRAR_USUARIO_REQUEST:
            return Object.assign({}, state, {
                autenticando: true,
                usuario: null,
                inicializando: true,
            });
        case CADASTRAR_ERROR:
            return Object.assign({}, state, {
                erro: action.payload,
                autenticando: false,
            });
        case ATUALIZAR_SUCCESS:
            return Object.assign({}, state, {
                usuario: action.payload,
                autenticando: false,
            });
        case LOGOUT:
            return Object.assign({}, state, {
                usuario: null,
                autenticando: false,
                inicializando: false,
            });
        case CARREGAR_LISTA_BOLOES_INSCRITO_REQUEST:
            return Object.assign({}, state, {
                carregandoBoloes: true
            });
        case CARREGAR_LISTA_BOLOES_INSCRITO_SUCCESS:
            return Object.assign({}, state, {
                carregandoBoloes: false,
                boloes_inscrito: action.payload
            });
        case CARREGAR_LISTA_BOLOES_INSCRITO_ERROR:
            return Object.assign({}, state, {
                carregandoBoloes: false
            });
        default:
            return state;
    }
}
