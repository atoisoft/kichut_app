import {carregar_partida, carregar_rodada} from '../../redux/actions/common';

export const RODADA_PROXIMA = 'RODADA_PROXIMA';
export const RODADA_ANTERIOR = 'RODADA_ANTERIOR';
export const PARTIDA_PROXIMA = 'PARTIDA_PROXIMA';
export const PARTIDA_ANTERIOR = 'PARTIDA_ANTERIOR';
export const SELECIONAR_PARTIDA = 'SELECIONAR_PARTIDA';

export function rodadaProxima(bolao_id, rodada_id, index) {
    return dispatch => {
        dispatch(carregar_rodada(bolao_id, rodada_id));
        dispatch({type: RODADA_PROXIMA, payload: index});
    };
}

export function rodadaAnterior(bolao_id, rodada_id, index) {
    return dispatch => {
        dispatch(carregar_rodada(bolao_id, rodada_id));
        dispatch({type: RODADA_ANTERIOR, payload: index});
    };
}

export function selecionarPartida(partida, index_partida, index_grupo) {
    return dispatch => {
        dispatch({
            type: SELECIONAR_PARTIDA,
            payload: {partida: partida, index_partida: index_partida, index_grupo: index_grupo}
        });
        dispatch(carregar_partida(partida.id));
    };
}

export function partidaProxima() {
    return dispatch => {
        dispatch({type: PARTIDA_PROXIMA});
    };
}

export function partidaAnterior() {
    return dispatch => {
        dispatch({type: PARTIDA_ANTERIOR});
    };
}
