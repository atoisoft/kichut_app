import React, {Component} from 'react';
import {Container, Root, StyleProvider, Text, View} from 'native-base';
import type {Notification} from 'react-native-firebase';
import firebase from 'react-native-firebase';
import {AsyncStorage, Alert, StatusBar, Platform} from 'react-native';
import getTheme from '../native-base-theme/components/index';
import {Provider} from 'react-redux';
import configureStore from './Store';
import BaseNavigation from './Navigation';
import CodePush from 'react-native-code-push';

type Props = {};

class App extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            store: configureStore(() => this.setState({isLoading: false})),
            showDownloadingModal: false,
            showInstalling: false,
            downloadProgress: 0
        };
    }

    async componentDidMount() {
        CodePush.sync(
            {updateDialog: false, installMode: CodePush.InstallMode.IMMEDIATE},
            status => {
                switch (status) {
                    case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
                        this.setState({showDownloadingModal: true});
                        break;
                    case CodePush.SyncStatus.INSTALLING_UPDATE:
                        this.setState({showInstalling: true});
                        break;
                    case CodePush.SyncStatus.UPDATE_INSTALLED:
                        break;
                    default:
                        break;
                }
            },
            ({receivedBytes, totalBytes}) => {
                this.setState({downloadProgress: receivedBytes / totalBytes * 100});
            }
        );
        this.checkPermission();
        this.createNotificationListeners(); //add this line
    }

    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('@Login:fsm_token');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('@Login:fsm_token', fcmToken);
            }
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }


    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const {title, body} = notification;
            this.showAlert(title, body);
        });

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const {title, body} = notificationOpen.notification;
            this.showAlert(title, body);
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const {title, body} = notificationOpen.notification;
            this.showAlert(title, body);
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.warn(JSON.stringify(message));
        });
    }

    showAlert(title, body) {
        Alert.alert(
            title, body,
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
        );
    }


    render() {
        return (
            <Root style={{flex: 1}}>
                <StatusBar hidden={true}/>
                <StyleProvider style={getTheme()}>
                    <Provider store={this.state.store}>
                        {this.state.showInstalling || this.state.showDownloadingModal
                            ? <Container style={{flex: 1, backgroundColor: '#2c3e50', alignItem: 'center', justifyContent: 'center'}}>
                                <Text style={{color: "#fff", textAlign: "center", marginBottom: 15, fontSize: 15}}>Instalando atualização...</Text>
                            </Container>
                            : <BaseNavigation/>}
                    </Provider>
                </StyleProvider>
            </Root>
        );
    }
}

let codePushOptions = {checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME};
export default CodePush(codePushOptions)(App);
