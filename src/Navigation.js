import React from 'react';
import {createAppContainer, createStackNavigator, createSwitchNavigator, createBottomTabNavigator} from "react-navigation";
import LoaderScreen from "./screens/LoadingScreen";
import LoginScreen from './screens/LoginScreen';
import CadastroScreen from './screens/CadastroScreen';
import HomeScreen from './screens/HomeScreen';
import ConfiguracaoScreen from './screens/ConfiguracaoScreen';
import ClassificacaoScreen from './screens/ClassificacaoScreen';
import RecuperarSenhaScreen from './screens/RecuperarSenhaScreen';
import ChuteScreen from './screens/ChuteScreen';
import PageFooter from "./components/PageFooter";
import PageHeader from "./components/PageHeader";

const AuthStack = createStackNavigator({
    Login: {screen: LoginScreen},
    Cadastro: {screen: CadastroScreen},
    RecuperarSenha: {screen: RecuperarSenhaScreen},
    Chute: {
        screen: ChuteScreen,
        swipeEnabled: false,
        navigationOptions: {
            swipeEnabled: false,
            gesturesEnabled: false,
        },
        headerMode: 'none'
    }
}, {
    headerMode: 'none',
    swipeEnabled: false,
    navigationOptions: {
        gesturesEnabled: false,
        headerStyle: {
            backgroundColor: '#081621', // this will handle the cutOff at the top the screen
        },
    },
});

const HomeStack = createBottomTabNavigator({
    Home: HomeScreen,
    Classificacao: {screen: ClassificacaoScreen},
    Configuracao: {screen: ConfiguracaoScreen},
}, {
    headerMode: 'none',
    initialRouteName: 'Home',
    tabBarPosition: 'bottom',
    tabBarOptions: {},
    tabBarComponent: props => <PageFooter  {...props} />,
    swipeEnabled: false,
    navigationOptions: {
        gesturesEnabled: false,
        headerStyle: {
            backgroundColor: '#081621', // this will handle the cutOff at the top the screen
        },
    },
});

const AppStack = createStackNavigator({
    HomeStack: {
        screen: HomeStack,
        navigationOptions: {
            headerTitle: <PageHeader/>
        },
        headerMode: 'none'
    },
}, {
    headerMode: 'screen',
    initialRouteName: 'HomeStack'
});


const BaseNavigation = createSwitchNavigator({
    LoaderScreen: LoaderScreen,
    Auth: AuthStack,
    App: AppStack
}, {
    headerMode: 'none',
    initialRouteName: 'LoaderScreen'
});

export default createAppContainer(BaseNavigation);
