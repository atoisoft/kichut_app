import {combineReducers} from 'redux';

import homeReducer from './reducer';
import authReducer from '../reducers/auth';

export default combineReducers({
    homeReducer,
    authReducer
});
