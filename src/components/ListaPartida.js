import * as React from 'react';
import {connect} from 'react-redux';
import {
    Icon,
    View
} from 'native-base';
import {Image, Text, TouchableHighlight} from 'react-native';
import * as utils from '../utils/functions';
import {
    selecionarPartida
} from '../screens/HomeScreen/actions';
import {bindActionCreators} from "redux";

class Equipe extends React.Component {
    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                padding: 2
            }}>
                {this.props.left &&
                <View style={{justifyContent: 'flex-start'}}>
                    {this.props.icone && <Image source={{uri: this.props.icone}} style={{height: 24, width: 24}}/>}
                </View>}
                <Text style={{color: '#e9e9ea', padding: 5, width: 95, textAlign: 'center'}}>{this.props.nome}</Text>
                {this.props.right &&
                <View style={{justifyContent: 'flex-end'}}>
                    {this.props.icone && <Image source={{uri: this.props.icone}} style={{height: 24, width: 24}}/>}
                </View>}
            </View>
        );
    }
}

class Placar extends React.Component {
    render() {
        return (
            <View style={{
                flex: 0.5,
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: -7,
                marginBottom: -7
            }}>
                {(this.props.status == '3' || this.props.status == 3) && <Text style={{
                    flex: 0.5,
                    textAlign: 'center',
                    justifyContent: 'center',
                    color: '#fff659',
                    fontWeight: 'bold',
                    fontSize: 8
                }}>Encerrado</Text>}
                {(this.props.status == '1' || this.props.status == 1) && <Text style={{
                    flex: 0.5,
                    textAlign: 'center',
                    justifyContent: 'center',
                    color: '#67ff4f',
                    fontWeight: 'bold',
                    fontSize: 8
                }}>Andamento</Text>}
                {this.props.status != '1' && this.props.status != '3' && <Text style={{flex: 0.5}}></Text>}
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            flex: 1,
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: 'white',
                            fontWeight: 'bold'
                        }}>{this.props.palpite_casa}</Text>
                        <Text
                            style={{
                                flex: 1,
                                textAlign: 'center',
                                justifyContent: 'center',
                                color: 'white',
                                fontSize: 10
                            }}>X</Text>
                        <Text style={{
                            flex: 1,
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: 'white',
                            fontWeight: 'bold'
                        }}>{this.props.palpite_fora}</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            flex: 1,
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff659',
                            fontWeight: 'bold',
                            fontSize: 8
                        }}>{this.props.gols_casa}</Text>
                        <Text
                            style={{
                                flex: 1,
                                textAlign: 'center',
                                justifyContent: 'center',
                                color: '#fff659',
                                fontSize: 6
                            }}>X</Text>
                        <Text style={{
                            flex: 1,
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff659',
                            fontWeight: 'bold',
                            fontSize: 8
                        }}>{this.props.gols_fora}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

export interface Props {
    navigation: any;
    dados: any;
    onClickPartida: any;
    isAoVivo: boolean;
}

export interface State {
}


class ListaPartida extends React.Component<Props, State> {

    onClickPartida(item, index, grupo) {
        this.props.selecionarPartida(item, index, grupo);
        if (this.props.onClickPartida != null) {
            this.props.onClickPartida(item);
        }
    }

    getPalpiteCasa(partida_id) {
        const palpite = this.props.meus_palpites && this.props.meus_palpites.find(it => it.partida === partida_id);
        if (palpite) {
            return palpite.gols_equipe_casa;
        } else {
            return '-';
        }
    }

    getPalpiteFora(partida_id) {
        const palpite = this.props.meus_palpites && this.props.meus_palpites.find(it => it.partida === partida_id);
        if (palpite) {
            return palpite.gols_equipe_fora;
        } else {
            return '-';
        }
    }

    renderPartidas(partidas, grupo = null) {
        return partidas && partidas.map((item, i) => {
            return (
                <TouchableHighlight key={i} onPress={() => this.onClickPartida(item, i, grupo)}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        height: 50,
                        borderColor: '#333d45',
                        borderBottomWidth: 0.8,
                    }}>
                        <View style={{flex: 1, flexDirection: 'row', padding: 5, marginRight: 5}}>
                            <View style={{justifyContent: 'center', marginRight: 15, alignItems: 'center'}}>
                                <Icon style={{color: 'white', fontSize: 16}} name="ios-clock"/>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 10
                                }}>{utils.exibirHora(item.datahora_partida)}</Text>
                            </View>
                            <Equipe left={true} nome={item.equipe_casa.nome} icone={item.equipe_casa.icone}/>
                            <Placar status={item.api_status} palpite_casa={this.getPalpiteCasa(item.id)}
                                    gols_casa={item.gols_equipe_casa}
                                    palpite_fora={this.getPalpiteFora(item.id)} gols_fora={item.gols_equipe_fora}/>
                            <Equipe right={true} nome={item.equipe_fora.nome} icone={item.equipe_fora.icone}/>
                        </View>
                        <View style={{flex: 0.1, marginLeft: 5}}>
                            <Icon style={{color: 'white'}} name="ios-arrow-dropright"/>
                        </View>
                    </View>
                </TouchableHighlight>
            );
        });
    }

    renderDefault() {
        if (this.props.rodadaSelecionada && !this.props.partidas_agrupadas) {
            return (
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: 'white', textAlign: 'center', fontSize: 18, margin: 10}}>Partidas não disponíveis</Text>
                </View>
            );
        }
        return this.props.rodadaSelecionada && this.props.partidas_agrupadas.map((item, index) => {
            return (
                <View key={index}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        backgroundColor: '#2e373e',
                        alignItems: 'center'
                    }}>
                        <View>
                            <Icon style={{color: '#97b4cd', padding: 7, fontSize: 20}} name="md-calendar"/>
                        </View>
                        <View>
                            <Text
                                style={{color: '#97b4cd', fontSize: 13}}>{utils.exibirDataPorExtenso(item.data)}</Text>
                        </View>
                    </View>
                    {this.renderPartidas(item.partidas, index)}
                </View>);
        });
    }

    render() {
        return (
            <View style={{flex: 1}}>
                {this.props.carregandoBolao || this.props.carregandoRodada ?
                    <Text></Text> : this.renderDefault()}
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({selecionarPartida}, dispatch);
const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    partidas: state.homeReducer.partidas,
    partidas_agrupadas: state.homeReducer.partidas_agrupadas,
    meus_palpites: state.homeReducer.meus_palpites,
    rodadaSelecionada: state.homeReducer.rodadaSelecionada,
    carregandoBolao: state.homeReducer.carregandoBolao,
    carregandoRodada: state.homeReducer.carregandoRodada,
});
export default connect(mapStateToProps, mapDispatchToProps)(ListaPartida);
