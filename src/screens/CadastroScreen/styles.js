import {StyleSheet} from 'react-native';

const styles: any = StyleSheet.create({
    container: {
        backgroundColor: '#081621',
    },
    row: {
        flex: 1,
        alignItems: 'center',
    },
    text: {
        fontSize: 20,
        marginBottom: 15,
        alignItems: 'center',
    },
    mt: {
        marginTop: 18,
    },
    title: {
        color: 'white'
    },
    input: {
        padding: 5,
        backgroundColor: '#507091',
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 10,
        color: '#fff'
    }
});
export default styles;
