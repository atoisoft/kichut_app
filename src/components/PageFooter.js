import * as React from 'react';
import {
    Button,
    Icon,
    Footer,
    FooterTab
} from 'native-base';
import {Text, Image} from 'react-native';
import {connect} from 'react-redux';

const ChutIcon = require('../assets/images/chut_icon.png');

export interface Props {
    navigation: any;
}

class PageFooter extends React.Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            tabActive: 'Home'
        };
    }

    renderChuts(focused) {
        return (<Button key={0} active={focused} vertical onPress={() => this.props.navigation.navigate('Home')}>
                <Image style={{width: 25, height: 24}} source={ChutIcon}/>
                <Text style={{
                    fontWeight: 'bold',
                    color: 'white',
                    fontSize: 10,
                    padding: 0,
                    paddingTop: 5,
                    marginTop: 0
                }} numberOfLines={1} ellipsizeMode={'tail'}>CHUTS</Text>
            </Button>
        );
    }

    renderClassificacao(focused) {
        return (
            <Button key={1} vertical onPress={() => this.props.navigation.navigate('Classificacao')}
                    active={focused}>
                <Icon style={{color: 'white'}} name="ios-list"/>
                <Text style={{
                    fontWeight: 'bold',
                    color: 'white',
                    fontSize: 10,
                    padding: 0,
                    paddingTop: 5,
                    marginTop: 0
                }} numberOfLines={1} ellipsizeMode={'tail'}>CLASSIFICAÇÃO</Text>
            </Button>
        );
    }

    renderConfiguracoes(focused) {
        return (
            <Button key={3} vertical onPress={() => this.props.navigation.navigate('Configuracao')}
                    active={focused}>
                <Icon style={{color: 'white'}} name="ios-build"/>
                <Text paddingHorizontal={0} style={{
                    fontWeight: 'bold',
                    color: 'white',
                    fontSize: 10,
                    padding: 0,
                    paddingTop: 5,
                    marginTop: 0
                }} numberOfLines={1} ellipsizeMode={'tail'}>CONFIGURAÇÕES</Text>
            </Button>
        );
    }

    render() {
        const {routes} = this.props.navigation.state;
        return (
            <Footer style={{backgroundColor: '#081621', borderColor: '#050f16'}}>
                <FooterTab>
                    {routes && routes.map((route, index) => {
                        const focused = index === this.props.navigation.state.index;
                        if (index === 0) {
                            return this.renderChuts(focused);
                        }
                        if (index === 1) {
                            return this.renderClassificacao(focused);
                        }
                        if (index === 2) {
                            return this.renderConfiguracoes(focused);
                        }
                    })}
                </FooterTab>
            </Footer>
        );
    }
}

function bindAction(dispatch) {
    return {};
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps, bindAction)(PageFooter);
