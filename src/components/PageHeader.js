import * as React from 'react';
import {View} from 'native-base';
import {Image, Dimensions, Platform} from 'react-native';
import {isIphoneX} from "../utils/functions";

const {width} = Dimensions.get('window');

const HeaderLogo = require('../assets/images/header_logo.png');

export interface Props {
}

export default class PageHeader extends React.Component<Props> {
    render() {
        return (
            <View style={{
                flex: 1,
                padding: 0,
                margin: 0,
                alignItems: 'center',
                backgroundColor: '#081621',
                marginTop: Platform.OS === 'ios' ? -20 : 0
            }}>
                <View style={{flex: 1, backgroundColor: '#081621',}}>
                    <Image source={HeaderLogo}
                           style={{
                               padding: 0,
                               margin: 0,
                               resizeMode: 'contain',
                               width: width,
                               height: 55,
                               top: isIphoneX() ? 7 : 0
                           }}/>
                </View>
            </View>
        );
    }
}
