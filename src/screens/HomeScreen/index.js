import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {AsyncStorage, RefreshControl} from 'react-native';
import {Container, Content, View} from 'native-base';
import {addFsmToken} from '../../redux/actions/auth';
import {Text} from 'native-base';
import BolaoHeader from "../../components/BolaoHeader";
import RodadaSelect from "../../components/RodadaSelect";
import ListaPartida from "../../components/ListaPartida";
import styles from './styles';
import {carregar_rodada} from '../../redux/actions/common';
import {verificarMaximoPalpites} from "../../utils/functions";

export interface Props {
    navigation: any
}

export interface State {
}

class HomeScreen extends React.Component<Props, State> {
    async componentDidMount() {
        const device_token_fsm = await AsyncStorage.getItem('@Login:fsm_token');
        this.props.addFsmToken(device_token_fsm);
    }

    maximoPalpites() {
        if (this.props.meus_palpites == null) {
            return;
        }
        const num = verificarMaximoPalpites(this.props.partidas, this.props.meus_palpites, this.props.bolao.max_placares_repetidos);
        return num > 0 ? (
            <View style={{backgroundColor: '#ff371a', padding: 10}}>
                <Text style={{color: 'white'}}>ATENÇÃO! Você tem {num} placares repetidos!</Text>
            </View>
        ) : null;
    }

    render() {
        return (
            <Container style={styles.container}>
                <BolaoHeader/>
                <RodadaSelect/>
                <Content refreshControl={
                    <RefreshControl refreshing={this.props.carregandoRodada} onRefresh={() => this.props.carregar_rodada(this.props.bolao.id, this.props.rodadaSelecionada.id)}/>
                }>
                    <ListaPartida onClickPartida={() => {
                        this.props.navigation.navigate('Chute');
                    }}/>
                </Content>
                {this.maximoPalpites()}
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({carregar_rodada, addFsmToken}, dispatch);
const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    usuario: state.authReducer.usuario,
    carregandoRodada: state.homeReducer.carregandoRodada,
    rodadaSelecionada: state.homeReducer.rodadaSelecionada,
    partidas: state.homeReducer.partidas,
    meus_palpites: state.homeReducer.meus_palpites,
});
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
