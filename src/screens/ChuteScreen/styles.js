import {StyleSheet} from 'react-native';

const styles: any = StyleSheet.create({
    container: {
        backgroundColor: '#283036',
    },
    tabContainer: {
        backgroundColor: '#283036',
        elevation: 0
    },
    separadorBox: {
        flex: 0.3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ultimosJogosSeparador: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 25,
        padding: 10,
    },
    ultimosJogosIconBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 8
    },
    aproveitamentoSeparador: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 25,
        padding: 10,
        paddingTop: 25
    },
    aproveitamentoBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 8,
        paddingTop: 4,
        paddingBottom: 4,
        marginBottom: 4
    },
    aproveitamentoMainBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#283036',
        paddingLeft: 5,
        paddingRight: 5
    },
    tabItemTitle: {
        backgroundColor: '#2e373e',
        padding: 5,
        borderBottomWidth: 1,
        borderColor: '#333d45',
    },
    tabItemTitleText: {
        color: '#97b4cd'
    },
    ultimosJogosBox: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#283036',
        paddingLeft: 5,
        paddingRight: 5
    },
    estatisticasBox: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#283036',
        paddingLeft: 5,
        paddingRight: 5
    },
    btnPlacarChute: {
        width: 40,
        height: 40,
        borderRadius: 30,
        justifyContent: 'center',
        borderColor: 'white',
        borderWidth: 2,
        margin: 5
    },
    btnPlacarChuteText: {
        fontSize: 22,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    emblemaView: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    emblemaText: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    emblemaImage: {height: 50, width: 50},
    input: {
        borderWidth: 1,
        borderColor: '#fff',
        padding: 5,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 10,
        color: '#fff'
    }
});
export default styles;
