import Http from './api';
import {Dimensions, Platform} from 'react-native';

const moment = require('moment');
require('moment/locale/pt-br');

export function exibirDataPorExtenso(data) {
    return moment(data, 'YYYY-MM-DD').format('dddd, DD [de] MMMM [de] YYYY');
}

export function exibirHora(datahora) {
    return moment(datahora, 'YYYY-MM-DD HH:mm:ssZ').format('HH:mm');
}

export function data(datahora) {
    return moment(datahora, 'YYYY-MM-DD HH:mm:ssZ').format('YYYY-MM-DD');
}

export function genericRequestPost(url, data, request, success, error, callbackSuccess) {
    return (dispatch) => {
        dispatch({type: request, data: data});
        Http.post(url, data, (result) => {
            dispatch({
                type: success,
                payload: result
            });
            if (callbackSuccess != null) {
                callbackSuccess(result, dispatch);
            }
        }, (result) => {
            dispatch({
                type: error,
                payload: result
            });
        });
    };
}

export function genericRequestPatch(url, data, request, success, error, callbackSuccess) {
    return (dispatch) => {
        dispatch({type: request, data: data});
        Http.patch(url, data, (result) => {
            dispatch({
                type: success,
                payload: result
            });
            if (callbackSuccess != null) {
                callbackSuccess(result, dispatch);
            }
        }, (result) => {
            dispatch({
                type: error,
                payload: result
            });
        });
    };
}

export function genericRequestGet(url, request, success, error, callbackSuccess) {
    return (dispatch) => {
        dispatch({type: request, payload: url});
        Http.get(url, (result) => {
            dispatch({
                type: success,
                payload: result
            });
            if (callbackSuccess != null) {
                callbackSuccess(result, dispatch);
            }
        }, (result) => {
            dispatch({
                type: error,
                payload: result
            });
        });
    };
}

export function verificarMaximoPalpites(partidas, meus_palpites, max_placares_repetidos) {
    if (max_placares_repetidos == 0) {
        return 0;
    }
    if (!meus_palpites) {
        return;
    }
    let palpites = [];
    partidas.map(it => {
        const palpite = meus_palpites.find(p => p.partida == it.id);
        if (palpite) {
            palpites.push(palpite);
        }
    });
    const repeticao = {};
    palpites.forEach(it => {
        const index = it.gols_equipe_casa.toString() + 'x' + it.gols_equipe_fora.toString();
        if (repeticao[index] == null) {
            repeticao[index] = 1;
        } else {
            repeticao[index] = repeticao[index] + 1;
        }
    });

    let num_repeticao = 0;
    const keys = Object.keys(repeticao);
    keys.forEach(it => {
        if (repeticao[it] > max_placares_repetidos) {
            num_repeticao = repeticao[it];
            return repeticao[it];
        }
    });
    return num_repeticao;
}

export function isIphoneX() {
    const dim = Dimensions.get('window');

    return (
        // This has to be iOS
        Platform.OS === 'ios' &&

        // Check either, iPhone X or XR
        (isIPhoneXSize(dim) || isIPhoneXrSize(dim))
    );
}

export function isIPhoneXSize(dim) {
    return dim.height == 812 || dim.width == 812;
}

export function isIPhoneXrSize(dim) {
    return dim.height == 896 || dim.width == 896;
}
