import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {View, Text, Container, Button} from "native-base";
import {KeyboardAvoidingView, Image, TextInput, TouchableHighlight, AsyncStorage} from "react-native";
import {VERSAO} from '../../utils/api';
import {authenticate} from "../../redux/actions/auth";
import styles from './styles';
import {isIphoneX} from "../../utils/functions";

const logo = require('../../assets/images/logo.png');

class LoginScreen extends React.Component {
    static navigationOptions = {};

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    componentDidMount = async () => {
        const username = await AsyncStorage.getItem('@Login:username');
        if (username) {
            this.setState({username});
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.usuario !== null) {
            this.props.navigation.navigate('App');
        }
    }

    onClickLogin = async () => {
        await AsyncStorage.setItem('@Login:username', this.state.username);

        if (this.props.autenticando) {
            return;
        }

        this.props.authenticate(this.state.username, this.state.password);
    };
    onClickCadastrar = async () => {
        this.props.navigation.navigate('Cadastro');
    };

    onClickRecuperarSenha = async () => {
        this.props.navigation.navigate('RecuperarSenha');
    };

    render() {
        return (
            <Container style={{backgroundColor: '#081621'}}>
                <View padder
                      style={{alignItems: 'center', flex: 0.8, justifyContent: 'center', marginTop: 20}}>
                    <Image source={logo} style={{width: 220, height: 220}}/>
                </View>
                <View padder style={{backgroundColor: 'transparent', flex: 1, margin: 0}}>
                    <View padder>
                        <TextInput style={styles.input} autoCapitalize="none"
                                   ref={(input) => this.usernameInput = input}
                                   onSubmitEditing={() => this.passwordInput.focus()}
                                   onChangeText={(username) => this.setState({username})}
                                   value={this.state.username}
                                   autoCorrect={false}
                                   keyboardType='default'
                                   returnKeyType="next"
                                   placeholder='Nome do usuário'
                                   placeholderTextColor='rgba(225,225,225,0.7)'/>

                        <TextInput style={styles.input} returnKeyType="go"
                                   ref={(input) => this.passwordInput = input}
                                   onChangeText={(password) => this.setState({password})}
                                   placeholder='Senha'
                                   placeholderTextColor='rgba(225,225,225,0.7)'
                                   secureTextEntry/>
                    </View>
                    <View padder>
                        <TouchableHighlight onPress={this.onClickRecuperarSenha}>
                            <Text style={{color: 'white', alignSelf: 'flex-end'}}>Esqueci a senha</Text>
                        </TouchableHighlight>
                    </View>
                    <KeyboardAvoidingView style={{flex: 1}} behavior={'height'}>
                        <View padder style={{flex: 0.8}}>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
                                <Button bordered
                                        style={{
                                            flex: 1,
                                            marginRight: 10,
                                            borderColor: 'white',
                                            borderRadius: 10,
                                            height: 40
                                        }} block
                                        onPress={this.onClickLogin.bind(this)}>
                                    <Text style={{color: 'white'}}>ENTRAR</Text>
                                </Button>
                                <Button bordered style={{
                                    flex: 1,
                                    marginLeft: 10,
                                    borderColor: 'white',
                                    borderRadius: 10,
                                    height: 40
                                }} block
                                        onPress={this.onClickCadastrar.bind(this)}>
                                    <Text style={{color: 'white'}}>CADASTRAR</Text>
                                </Button>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </View>
                <View><Text style={{
                    color: 'white',
                    textAlign: 'right',
                    marginRight: isIphoneX() ? 35 : 5,
                    marginBottom: 3,
                    fontSize: 12
                }}>v: {VERSAO}</Text></View>
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({authenticate}, dispatch);

const mapStateToProps = state => ({
    usuario: state.authReducer.usuario,
    autenticando: state.authReducer.autenticando,
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
