import {StyleSheet} from 'react-native';

const styles: any = StyleSheet.create({
    container: {
        backgroundColor: '#283036',
    },
    tabContainer: {
        backgroundColor: '#283036',
        elevation: 0
    },
    table: {
        flexDirection: 'row',
        padding: 5,
        borderBottomWidth: 0.8,
        borderColor: '#333d45',
        backgroundColor: '#2e373e',
        alignItems: 'center'
    }
});
export default styles;
