import * as React from 'react';
import {connect} from 'react-redux';
import {RefreshControl, ART, TextInput} from 'react-native';
import {Container, Content, Tab, Tabs, Icon, Toast, Button} from 'native-base';
import {Image, Text, TouchableHighlight, Dimensions, View, Modal} from 'react-native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';

import styles from './styles';
import {partidaProxima, partidaAnterior} from '../../screens/HomeScreen/actions';
import {cadastrar_palpite, carregar_partida} from '../../redux/actions/common';
import {isIphoneX} from "../../utils/functions";

const {Surface, Shape} = ART;

const BgChute = require('../../assets/images/estadio.jpg');

const WINDOW = Dimensions.get('window');

export interface Props {
}

export interface State {
}

class IconeUltimoJogo extends React.Component {
    render() {
        return (
            <View>
                {this.props.value === 1 && <View style={{
                    backgroundColor: '#009c3b',
                    width: 25,
                    height: 25,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 25
                }}>
                    <Text style={{textAlign: 'center', color: 'white'}}>V</Text>
                </View>}
                {this.props.value === 0 && <View style={{
                    backgroundColor: '#ffdf00',
                    width: 25,
                    height: 25,
                    alignItems: 'center', justifyContent: 'center',
                    borderRadius: 25
                }}>
                    <Text style={{textAlign: 'center', color: 'white'}}>E</Text>
                </View>}
                {this.props.value === 2 && <View style={{
                    backgroundColor: '#9c0f00',
                    width: 25,
                    height: 25,
                    alignItems: 'center', justifyContent: 'center',
                    borderRadius: 25
                }}>
                    <Text style={{textAlign: 'center', color: 'white'}}>X</Text>
                </View>}
                {this.props.value == null && <View style={{
                    backgroundColor: '#61829c',
                    width: 25,
                    height: 25,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 25
                }}>
                    <Text style={{textAlign: 'center', color: 'white'}}>-</Text>
                </View>}
            </View>
        );
    }
}

class Aproveitamento extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            valor: 0
        };
    }

    componentWillMount() {
        this.setState({valor: this.props.fill});
    }

    clear() {
        this.setState({valor: 0});
    }

    fill() {
        this.setState({valor: this.props.fill});
    }

    render() {
        return (
            <View>
                <Text style={{textAlign: 'center', color: 'white', marginBottom: 6}}>{this.props.nome}</Text>
                <AnimatedCircularProgress
                    size={40}
                    width={5}
                    fill={this.state.valor}
                    tintColor="#00e0ff"
                    backgroundColor="#3d5875">
                    {
                        (fill) => (
                            <Text style={{color: 'white'}}>{this.state.valor.toFixed(0)}</Text>
                        )
                    }
                </AnimatedCircularProgress>
            </View>
        );
    }
}

class Chute extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            chuteCasa: 0,
            chuteFora: 0,
            mudandoPalpiteCasa: false,
            mudandoPalpiteFora: false,
            modalVisible: false,
            saindo: false,
            palpite: null
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.partidaSelecionada != null && nextProps.partidaSelecionada.id !== this.props.partidaSelecionada.id) {
            this.props.carregar_partida(nextProps.partidaSelecionada.id);
        }
    }

    getPalpiteCasa(partida_id) {
        const palpite = this.props.meus_palpites && this.props.meus_palpites.find(it => it.partida === partida_id);
        if (palpite) {
            return palpite.gols_equipe_casa;
        } else {
            return -1;
        }
    }

    getPalpiteFora(partida_id) {
        const palpite = this.props.meus_palpites && this.props.meus_palpites.find(it => it.partida === partida_id);
        if (palpite) {
            return palpite.gols_equipe_fora;
        } else {
            return -1;
        }
    }

    mudarChuteCasa(placar) {
        if (this.props.partidaSelecionada.ja_realizada) {
            Toast.show({
                text: 'Partida já realizada ou em andamento',
                buttonText: 'OK'
            });
            return;
        }
        const palpite = {
            partida: this.props.partidaSelecionada.id,
            gols_equipe_casa: placar,
            gols_equipe_fora: this.getPalpiteFora(this.props.partidaSelecionada.id)
        };
        this.props.cadastrar_palpite(this.props.bolao.id, palpite);
    }

    mudarChuteCasaTeclado() {
        this.setState({mudandoPalpiteCasa: true}, () => {
            this.setModalVisible(true);
        });
    }

    mudarChuteForaTeclado() {
        this.setState({mudandoPalpiteFora: true}, () => {
            this.setModalVisible(true);
        });
    }

    mudarChuteFora(placar) {
        if (this.props.partidaSelecionada.ja_realizada) {
            Toast.show({
                text: 'Partida já realizada ou em andamento',
                buttonText: 'OK'
            });
            return;
        }
        const palpite = {
            partida: this.props.partidaSelecionada.id,
            gols_equipe_casa: this.getPalpiteCasa(this.props.partidaSelecionada.id),
            gols_equipe_fora: placar
        };
        this.props.cadastrar_palpite(this.props.bolao.id, palpite);
    }

    partidaProxima() {
        if (!this.props.carregandoPartida) {
            this.props.partidaProxima();
        }
    }

    partidaAnterior() {
        if (!this.props.carregandoPartida) {
            this.props.partidaAnterior();
        }
    }

    onChangeTab(index) {
        if (!this._mandanteGeral) {
            return;
        }
        if (!this._visitanteGeral) {
            return;
        }
        if (index === 0) {
            this._mandanteGeral.fill();
            this._mandanteCasa.fill();
            this._mandanteFora.fill();

            this._visitanteGeral.fill();
            this._visitanteCasa.fill();
            this._visitanteFora.fill();
        } else {
            this._mandanteGeral.clear();
            this._mandanteCasa.clear();
            this._mandanteFora.clear();

            this._visitanteGeral.clear();
            this._visitanteCasa.clear();
            this._visitanteFora.clear();
        }
    }

    setModalVisible(visible) {
        if (!visible) {
            this.setState({mudandoPalpiteCasa: false, mudandoPalpiteFora: false});
        }
        this.setState({modalVisible: visible});
    }

    hide() {
        if (this.state.saindo) {
            return;
        }
        this.setState({saindo: true}, () => {
            this.props.navigation.navigate('Home');
        });
    }

    setPalpite() {
        if (this.state.palpite) {
            if (Number(this.state.palpite) < 0 || Number(this.state.palpite) > 100) {
                Toast.show({
                    text: 'Palpite inválido',
                    buttonText: 'OK'
                });
                return;
            }
        } else {
            Toast.show({
                text: 'Palpite inválido',
                buttonText: 'OK'
            });
            return;
        }

        if (this.state.mudandoPalpiteCasa) {
            this.mudarChuteCasa(this.state.palpite);
        }
        if (this.state.mudandoPalpiteFora) {
            this.mudarChuteFora(this.state.palpite);
        }
        this.setModalVisible(false);
    }

    existePartidaAntes() {
        const newIndex = this.props.partidaSelecionadaIndex - 1;
        let grupoNew = this.props.partidas_agrupadas[this.props.partidaGrupoSelecionadaIndex];
        if (grupoNew != null) {
            let partida = grupoNew.partidas[newIndex];
            if (partida != null) {
                return true;
            } else {//Não tem mais partida no grupo selecionado
                const newIndexGrupo = this.props.partidaGrupoSelecionadaIndex - 1;
                let grupoEncontrado = this.props.partidas_agrupadas[newIndexGrupo];

                return grupoEncontrado != null;
            }
        } else {
            return false;
        }
    }

    existePartidaDepois() {
        let newIndex = this.props.partidaSelecionadaIndex + 1;
        //Verificar se tem partida no grupo
        const grupo = this.props.partidas_agrupadas[this.props.partidaGrupoSelecionadaIndex];
        if (grupo != null) {
            let partida = grupo.partidas[newIndex];
            if (partida != null) {
                return true;
            } else {//Não tem mais partida no grupo selecionado
                const newIndexGrupo = this.props.partidaGrupoSelecionadaIndex + 1;
                let grupoEncontrado = this.props.partidas_agrupadas[newIndexGrupo];
                return grupoEncontrado != null;
            }
        }
        return false;
    }

    render() {
        const {partidaSelecionada} = this.props;
        return (
            <Container style={styles.container}>
                {partidaSelecionada && <View style={{backgroundColor: '#283036', flex: 1}}>
                    <Image source={BgChute}
                           resizeMode={'cover'}
                           opacity={0.60}
                           style={{
                               height: isIphoneX() ? 365 : 340,
                               width: WINDOW.width,
                               position: 'absolute'
                           }}/>
                    <View style={{
                        flex: 0.12,
                        flexDirection: 'row',
                        width: WINDOW.width,
                        paddingTop: isIphoneX() ? 23 : 5,
                        alignItems: 'center', justifyContent: 'center'
                    }}>
                        <View style={{flex: 1, alignItems: 'flex-start', paddingLeft: 25}}>
                            {this.existePartidaAntes() &&
                            <TouchableHighlight onPress={() => this.partidaAnterior()} underlayColor='transparent'>
                                <Icon style={{color: 'white', fontSize: 48}} name="ios-arrow-round-back"/>
                            </TouchableHighlight>}
                        </View>
                        <View style={{flex: 1, alignItems: 'center'}}>
                            <TouchableHighlight onPress={() => this.hide()} underlayColor='transparent'>
                                <Icon style={{color: 'white', fontSize: 36}} name="ios-home"/>
                            </TouchableHighlight>
                        </View>
                        <View style={{flex: 1, alignItems: 'flex-end', paddingRight: 25}}>
                            {this.existePartidaDepois() &&
                            <TouchableHighlight onPress={() => this.partidaProxima()} underlayColor='transparent'>
                                <Icon style={{color: 'white', fontSize: 48}}
                                      name="ios-arrow-round-forward"/>
                            </TouchableHighlight>}
                        </View>
                    </View>
                    <View style={{flex: 0.5, flexDirection: 'row'}}>
                        <View style={{flex: 1}}>
                            <View style={styles.emblemaView}>
                                {partidaSelecionada.equipe_casa.icone && <Image source={{uri: partidaSelecionada.equipe_casa.icone}}
                                                                                style={styles.emblemaImage}/>}
                                <Text style={styles.emblemaText}>{partidaSelecionada.equipe_casa.nome}</Text>
                            </View>
                            <View style={{
                                flex: 0.4,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteCasa(0)}>
                                    <Text style={styles.btnPlacarChuteText}>0</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteCasa(1)}>
                                    <Text style={styles.btnPlacarChuteText}>1</Text>
                                </TouchableHighlight>
                            </View>
                            <View style={{
                                flex: 0.4,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteCasa(2)}>
                                    <Text style={styles.btnPlacarChuteText}>2</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteCasa(3)}>
                                    <Text style={styles.btnPlacarChuteText}>3</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteCasaTeclado()}>
                                    <Text style={styles.btnPlacarChuteText}>+3</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                        <View style={{
                            flex: 0.5, flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingBottom: 60,
                        }}>
                            <View>
                                <Text
                                    style={{
                                        fontSize: 40,
                                        fontWeight: 'bold',
                                        color: 'white',
                                        backgroundColor: 'transparent'
                                    }}>{this.getPalpiteCasa(this.props.partidaSelecionada.id)}</Text>
                            </View>
                            <View><Text style={{fontSize: 30, color: 'white', backgroundColor: 'transparent'}}>x</Text></View>
                            <View>
                                <Text
                                    style={{
                                        fontSize: 40,
                                        fontWeight: 'bold',
                                        color: 'white',
                                        backgroundColor: 'transparent'
                                    }}>{this.getPalpiteFora(this.props.partidaSelecionada.id)}</Text>
                            </View>
                        </View>
                        <View style={{flex: 1}}>
                            <View style={styles.emblemaView}>
                                {partidaSelecionada.equipe_fora.icone && <Image source={{uri: partidaSelecionada.equipe_fora.icone}}
                                                                                style={styles.emblemaImage}/>}
                                <Text style={styles.emblemaText}>{partidaSelecionada.equipe_fora.nome}</Text>
                            </View>
                            <View style={{
                                flex: 0.4,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteFora(0)}>
                                    <Text style={styles.btnPlacarChuteText}>0</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteFora(1)}>
                                    <Text style={styles.btnPlacarChuteText}>1</Text>
                                </TouchableHighlight>
                            </View>
                            <View style={{
                                flex: 0.4,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteFora(2)}>
                                    <Text style={styles.btnPlacarChuteText}>2</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteFora(3)}>
                                    <Text style={styles.btnPlacarChuteText}>3</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.btnPlacarChute}
                                                    onPress={() => this.mudarChuteForaTeclado()}>
                                    <Text style={styles.btnPlacarChuteText}>+3</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <Content style={{flex: 1, marginTop: 10}} refreshControl={
                        <RefreshControl refreshing={this.props.carregandoPartida}
                                        onRefresh={() => this.props.carregar_partida(this.props.partidaSelecionada.id)}/>
                    }>
                        <Tabs initialPage={0} style={{flex: 1}}
                              onChangeTab={({i, ref, from}) => this.onChangeTab(i)}>
                            <Tab heading="Campeonato" style={styles.tabContainer}
                                 textStyle={{fontSize: 12, color: 'white'}}
                                 activeTextStyle={{fontSize: 12, color: 'white'}}>

                                {!this.props.carregandoPartida && <View>
                                    <View>
                                        <View style={styles.tabItemTitle}>
                                            <Text style={styles.tabItemTitleText}>Últimos jogos</Text>
                                        </View>
                                        <View style={styles.ultimosJogosBox}>
                                            <View style={styles.ultimosJogosIconBox}>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_casa.ultimos_jogos ? partidaSelecionada.equipe_casa.ultimos_jogos[0] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_casa.ultimos_jogos ? partidaSelecionada.equipe_casa.ultimos_jogos[1] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_casa.ultimos_jogos ? partidaSelecionada.equipe_casa.ultimos_jogos[2] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_casa.ultimos_jogos ? partidaSelecionada.equipe_casa.ultimos_jogos[3] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_casa.ultimos_jogos ? partidaSelecionada.equipe_casa.ultimos_jogos[4] : 0}/>
                                            </View>
                                            <View style={styles.separadorBox}>
                                                <Text style={styles.ultimosJogosSeparador}>-</Text>
                                            </View>
                                            <View style={styles.ultimosJogosIconBox}>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_fora.ultimos_jogos ? partidaSelecionada.equipe_fora.ultimos_jogos[4] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_fora.ultimos_jogos ? partidaSelecionada.equipe_fora.ultimos_jogos[3] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_fora.ultimos_jogos ? partidaSelecionada.equipe_fora.ultimos_jogos[2] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_fora.ultimos_jogos ? partidaSelecionada.equipe_fora.ultimos_jogos[1] : 0}/>
                                                <IconeUltimoJogo
                                                    value={partidaSelecionada.equipe_fora.ultimos_jogos ? partidaSelecionada.equipe_fora.ultimos_jogos[0] : 0}/>
                                            </View>
                                        </View>
                                    </View>
                                    <View>
                                        <View style={styles.tabItemTitle}>
                                            <Text style={styles.tabItemTitleText}>Aproveitamento no campeonato
                                                (%)</Text>
                                        </View>
                                        {partidaSelecionada.equipe_casa.aproveitamento && partidaSelecionada.equipe_fora.aproveitamento &&
                                        <View style={styles.aproveitamentoMainBox}>
                                            <View style={styles.aproveitamentoBox}>
                                                <Aproveitamento
                                                    fill={partidaSelecionada.equipe_casa.aproveitamento.geral}
                                                    nome='Geral'
                                                    ref={component => this._mandanteGeral = component}/>
                                                <Aproveitamento
                                                    fill={partidaSelecionada.equipe_casa.aproveitamento.casa}
                                                    nome='Casa'
                                                    ref={component => this._mandanteCasa = component}/>
                                                <Aproveitamento
                                                    fill={partidaSelecionada.equipe_casa.aproveitamento.fora}
                                                    nome='Fora'
                                                    ref={component => this._mandanteFora = component}/>
                                            </View>
                                            <View style={styles.SeparadorBox}>
                                                <Text style={styles.aproveitamentoSeparador}>-</Text>
                                            </View>
                                            <View style={styles.aproveitamentoBox}>
                                                <Aproveitamento
                                                    fill={partidaSelecionada.equipe_fora.aproveitamento.geral}
                                                    nome='Geral'
                                                    ref={component => this._visitanteGeral = component}/>
                                                <Aproveitamento
                                                    fill={partidaSelecionada.equipe_fora.aproveitamento.casa}
                                                    nome='Casa'
                                                    ref={component => this._visitanteCasa = component}/>
                                                <Aproveitamento
                                                    fill={partidaSelecionada.equipe_fora.aproveitamento.fora}
                                                    nome='Fora'
                                                    ref={component => this._visitanteFora = component}/>
                                            </View>
                                        </View>}
                                    </View>
                                    <View>
                                        <View style={styles.tabItemTitle}>
                                            <Text style={styles.tabItemTitleText}>Estatísticas dos jogos</Text>
                                        </View>
                                        <View style={styles.estatisticasBox}>
                                            <View style={{flex: 1, flexDirection: 'row', padding: 4}}>
                                                <Text style={{flex: 0.6, color: 'white', fontWeight: 'bold'}}>Time</Text>
                                                <Text style={{flex: 0.1, color: 'white', fontWeight: 'bold'}}>J</Text>
                                                <Text style={{flex: 0.1, color: 'white', fontWeight: 'bold'}}>V</Text>
                                                <Text style={{flex: 0.1, color: 'white', fontWeight: 'bold'}}>E</Text>
                                                <Text style={{flex: 0.1, color: 'white', fontWeight: 'bold'}}>D</Text>
                                                <Text style={{flex: 0.1, color: 'white', fontWeight: 'bold'}}>G</Text>
                                                <Text style={{flex: 0.1, color: 'white', fontWeight: 'bold'}}>S</Text>
                                            </View>
                                            {partidaSelecionada.equipe_casa.estatisticas &&
                                            <View style={{flex: 1, flexDirection: 'row', padding: 4}}>
                                                <Text style={{flex: 0.6, color: 'white'}}>{partidaSelecionada.equipe_casa.nome}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_casa.estatisticas.J}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_casa.estatisticas.V}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_casa.estatisticas.E}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_casa.estatisticas.D}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_casa.estatisticas.G}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_casa.estatisticas.S}</Text>
                                            </View>}
                                            {partidaSelecionada.equipe_fora.estatisticas &&
                                            <View style={{flex: 1, flexDirection: 'row', padding: 4}}>
                                                <Text style={{flex: 0.6, color: 'white'}}>{partidaSelecionada.equipe_fora.nome}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_fora.estatisticas.J}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_fora.estatisticas.V}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_fora.estatisticas.E}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_fora.estatisticas.D}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_fora.estatisticas.G}</Text>
                                                <Text style={{flex: 0.1, color: 'white'}}>{partidaSelecionada.equipe_fora.estatisticas.S}</Text>
                                            </View>}
                                        </View>
                                    </View>
                                </View>}
                            </Tab>
                            <Tab heading="Chuts" style={styles.tabContainer}
                                 tabStyle={{backgroundColor: 'transparent', elevation: 0}}
                                 activeTabStyle={{backgroundColor: 'transparent', elevation: 0}}
                                 textStyle={{fontSize: 12, color: 'white'}}
                                 activeTextStyle={{fontSize: 12, color: 'white'}}>
                                {!this.props.carregandoPartida && <View>
                                    <View style={styles.tabItemTitle}>
                                        <Text style={styles.tabItemTitleText}>Chutes</Text>
                                    </View>
                                    {!this.props.rodadaSelecionada.iniciada &&
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                        <Text style={{
                                            color: 'white',
                                            textAlign: 'center'
                                        }}>Aguarde o encerramento das apostas para ver os palpites dos outros
                                            chutadores</Text>
                                    </View>}
                                    {this.props.rodadaSelecionada.iniciada &&
                                    <View style={{flexDirection: 'row', marginTop: 5}}>
                                        <View style={{flex: 0.6}}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center',
                                                fontWeight: 'bold'
                                            }}>CHUTADOR</Text>
                                        </View>
                                        <View style={{flex: 0.2}}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center',
                                                fontWeight: 'bold'
                                            }}>CHUT</Text>
                                        </View>
                                        <View style={{flex: 0.2}}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center',
                                                fontWeight: 'bold'
                                            }}>PONTOS</Text>
                                        </View>
                                    </View>}
                                    {this.props.rodadaSelecionada.iniciada && partidaSelecionada.chuts && partidaSelecionada.chuts.map((item, index) => {
                                        return (<View key={index} style={{flexDirection: 'row'}}>
                                            <View style={{flex: 0.6, paddingLeft: 5}}>
                                                <Text style={{
                                                    color: 'white',
                                                    textAlign: 'left'
                                                }}>{item.chutador}</Text>
                                            </View>
                                            <View style={{flex: 0.2}}>
                                                <Text style={{
                                                    color: 'white',
                                                    textAlign: 'center'
                                                }}>{item.gols_equipe_casa} x {item.gols_equipe_fora}</Text>
                                            </View>
                                            <View style={{flex: 0.2}}>
                                                <Text style={{
                                                    color: 'white',
                                                    textAlign: 'center'
                                                }}>{item.pontos_fazendo}</Text>
                                            </View>
                                        </View>);
                                    })}
                                </View>}
                            </Tab>
                        </Tabs>
                    </Content>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setModalVisible(false);
                        }}>
                        <View style={{
                            height: 180,
                            flexDirection: 'column',
                            backgroundColor: '#283036',
                            margin: 30,
                            marginTop: 100,
                            marginBottom: 100, padding: 10, borderWidth: 0.75, borderColor: 'white'
                        }}>
                            <View style={{borderBottomWidth: 1, borderColor: '#333d45'}}>
                                <Text style={{
                                    color: 'white',
                                    marginBottom: 10,
                                    marginTop: 10,
                                    fontSize: 18,
                                    textAlign: 'center'
                                }}>Informe seu palpite</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <TextInput style={styles.input} autoCapitalize="none"
                                           ref={(input) => this.palpiteInput = input}
                                           onChangeText={(palpite) => this.setState({palpite})}
                                           value={this.state.palpite}
                                           autoCorrect={false}
                                           keyboardType='default'
                                           returnKeyType="next"
                                           placeholder='Palpite'
                                           placeholderTextColor='rgba(225,225,225,0.7)'/>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row'}}>
                                <Button block bordered style={{
                                    flex: 1,
                                    backgroundColor: '#5a5d5f',
                                    borderColor: 'white',
                                    marginRight: 10,
                                    height: 40,
                                }} onPress={() => this.setModalVisible(false)}><Text
                                    style={{color: 'white'}}>CANCELAR</Text></Button>
                                <Button block bordered style={{
                                    flex: 1,
                                    backgroundColor: '#009538',
                                    borderColor: 'white',
                                    marginLeft: 10,
                                    height: 40,
                                }} onPress={() => this.setPalpite()}><Text
                                    style={{color: 'white'}}>CONFIRMAR</Text></Button>
                            </View>
                        </View>
                    </Modal>
                </View>}
            </Container>
        );
    }
}

function bindAction(dispatch) {
    return {
        partidaProxima: () => dispatch(partidaProxima()),
        partidaAnterior: () => dispatch(partidaAnterior()),
        cadastrar_palpite: (bolao_id, data) => dispatch(cadastrar_palpite(bolao_id, data)),
        carregar_partida: (partida_id) => dispatch(carregar_partida(partida_id))
    };
}

const mapStateToProps = state => ({
    bolao: state.homeReducer.bolao,
    meus_palpites: state.homeReducer.meus_palpites,
    partidas_agrupadas: state.homeReducer.partidas_agrupadas,
    rodadaSelecionada: state.homeReducer.rodadaSelecionada,
    partidaSelecionada: state.homeReducer.partidaSelecionada,
    partidaSelecionadaIndex: state.homeReducer.partidaSelecionadaIndex,
    partidaGrupoSelecionadaIndex: state.homeReducer.partidaGrupoSelecionadaIndex,
    carregandoPartida: state.homeReducer.carregandoPartida
});
export default connect(mapStateToProps, bindAction)(Chute);
