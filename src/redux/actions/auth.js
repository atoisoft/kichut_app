import Http from '../../utils/api';
import {genericRequestGet, genericRequestPatch, genericRequestPost} from '../../utils/functions';
import {Alert, AsyncStorage} from 'react-native';
import {carregar_bolao} from './common';

export const LOGIN_URL = '/auth/login/';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const CLEAR_ERROR = 'CLEAR_ERROR';

export const CADASTRAR_USUARIO_REQUEST = 'CADASTRAR_USUARIO_REQUEST';
export const CADASTRAR_USUARIO_SUCCESS = 'CADASTRAR_USUARIO_SUCCESS';
export const CADASTRAR_USUARIO_ERROR = 'CADASTRAR_USUARIO_ERROR';

export const RECUPERAR_SENHA_REQUEST = 'RECUPERAR_SENHA_REQUEST';
export const RECUPERAR_SENHA_SUCCESS = 'RECUPERAR_SENHA_SUCCESS';
export const RECUPERAR_SENHA_ERROR = 'RECUPERAR_SENHA_ERROR';

export const ATUALIZAR_REQUEST = 'ATUALIZAR_REQUEST';
export const ATUALIZAR_SUCCESS = 'ATUALIZAR_SUCCESS';
export const ATUALIZAR_ERROR = 'ATUALIZAR_ERROR';

export const CARREGAR_USUARIO_REQUEST = 'CARREGAR_USUARIO_REQUEST';
export const CARREGAR_USUARIO_SUCCESS = 'CARREGAR_USUARIO_SUCCESS';
export const CARREGAR_USUARIO_ERROR = 'CARREGAR_USUARIO_ERROR';

export const CARREGAR_LISTA_BOLOES_INSCRITO_REQUEST = 'CARREGAR_LISTA_BOLOES_INSCRITO_REQUEST';
export const CARREGAR_LISTA_BOLOES_INSCRITO_SUCCESS = 'CARREGAR_LISTA_BOLOES_INSCRITO_SUCCESS';
export const CARREGAR_LISTA_BOLOES_INSCRITO_ERROR = 'CARREGAR_LISTA_BOLOES_INSCRITO_ERROR';

export const TOKEN_REQUEST = 'TOKEN_REQUEST';
export const TOKEN_SUCCESS = 'TOKEN_SUCCESS';
export const TOKEN_ERROR = 'TOKEN_ERROR';

export const LOGOUT = 'LOGOUT';

export function authenticate(email, password) {
    if (email === 'teste@kichut.com.br') {
        Http.setApple(true);
    }
    if (email === 'dev@kichut.com.br') {
        Http.setIsTeste();
    }
    Http.setAxiosAuthorization(null);
    const data = {
        email: email,
        password: password
    };
    return (dispatch) => {
        dispatch({type: LOGIN_REQUEST, payload: data});
        Http.post(LOGIN_URL, data, async (success) => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: success
            });
            carregarUsuario(success.user, dispatch);
        }, (error) => {
            dispatch({
                type: LOGIN_ERROR,
                payload: error
            });
        });
    };
}

export function logout() {
    return {
        type: LOGOUT
    };
}

export function clear_error() {
    return {
        type: CLEAR_ERROR
    };
}

export function cadastrar_usuario(values) {
    const URL = '/auth/registration/';
    return genericRequestPost(URL, values, CADASTRAR_USUARIO_REQUEST, CADASTRAR_USUARIO_SUCCESS, CADASTRAR_USUARIO_ERROR, async (success, dispatch) => {
        carregarUsuario(success.user, dispatch);
    });
}

export function recuperar_senha(values) {
    const URL = '/auth/password/reset/';
    return genericRequestPost(URL, values, RECUPERAR_SENHA_REQUEST, RECUPERAR_SENHA_SUCCESS, RECUPERAR_SENHA_ERROR, async (success, dispatch) => {
        Alert.alert('Sucesso', 'E-mail para recuperação de senha enviado');
    });
}

export function atualizar_usuario(values) {
    const URL = '/auth/user/';
    return genericRequestPatch(URL, values, ATUALIZAR_REQUEST, ATUALIZAR_SUCCESS, ATUALIZAR_ERROR, () => {
        Alert.alert('Sucesso', 'Perfil atualizado com sucesso');
    });
}

async function carregarUsuario(usuario, dispatch) {
    AsyncStorage.setItem('@Login:token', usuario.token.toString());

    //Se tiver somente um bolão, carregá-lo
    if (usuario.boloes_inscrito.length === 1) {
        dispatch(carregar_bolao(usuario.boloes_inscrito[0].bolao.id, usuario.boloes_inscrito[0].bolao.rodada_atual));
    }

    if (usuario.boloes_inscrito.length > 1) {
        try {
            const value = await AsyncStorage.getItem('@Bolao:selecionado_id');
            if (value !== null) {
                const inscricao = usuario.boloes_inscrito.find(it => it.bolao.id.toString() === value);
                dispatch(carregar_bolao(inscricao.bolao.id, inscricao.bolao.rodada_atual));
            } else {
                dispatch(carregar_bolao(usuario.boloes_inscrito[0].bolao.id, usuario.boloes_inscrito[0].bolao.rodada_atual));
            }
        } catch (error) {
            dispatch(carregar_bolao(usuario.boloes_inscrito[0].bolao.id, usuario.boloes_inscrito[0].bolao.rodada_atual));
        }
    }
}

export function ler_usuario(token) {
    const URL = '/auth/user/';
    Http.setAxiosAuthorization(token);

    return genericRequestGet(URL, CARREGAR_USUARIO_REQUEST, CARREGAR_USUARIO_SUCCESS, CARREGAR_USUARIO_ERROR, (success, dispatch) => {
        carregarUsuario(success, dispatch);
    });
}

export function carregarListaBoloesInscrito() {
    const URL = '/usuarios/meus_boloes/?app=1';
    return genericRequestGet(URL, CARREGAR_LISTA_BOLOES_INSCRITO_REQUEST, CARREGAR_LISTA_BOLOES_INSCRITO_SUCCESS, CARREGAR_LISTA_BOLOES_INSCRITO_ERROR);
}

export function addFsmToken(token) {
    const URL = '/token_device/';
    return genericRequestPost(URL, {'token_device': token}, TOKEN_REQUEST, TOKEN_SUCCESS, TOKEN_ERROR, async (success, dispatch) => {
    });
}
