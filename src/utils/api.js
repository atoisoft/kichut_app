import {Alert} from 'react-native';

import axios from 'axios';

export const VERSAO = '1.8';
const TIMEOUT = 20000;
let IS_PROD = true;
const instanceAxios = axios.create({
    headers: {
        'Accept': 'application/json',
        timeout: TIMEOUT,
        'Content-Type': 'application/json'
    }
});
let APPLE = false;

export default class Http {
    static setAxiosAuthorization(token) {
        if (token) {
            instanceAxios.defaults.headers.common['Authorization'] = 'Token ' + token;
        } else {
            delete instanceAxios.defaults.headers.common['Authorization'];
        }
    }

    static setApple(apple) {
        APPLE = apple;
    }

    static setIsTeste() {
        IS_PROD = false;
    }

    static setClientUrl() {
        if (IS_PROD) {
            instanceAxios.defaults.baseURL = 'https://api.kichut.com.br';
        } else {
            instanceAxios.defaults.baseURL = 'http://192.168.0.100:8000';
        }
        /*if (APPLE) {
            instanceAxios.defaults.baseURL = 'https://prod.api.kichut.com.br';
        }*/
    }

    static get(url, onSuccess, onError) {
        this.setClientUrl();
        instanceAxios.defaults.headers.common['Content-Type'] = 'application/json';
        instanceAxios.get(url, {timeout: TIMEOUT})
            .then(response => {
                onSuccess(response.data);
            })
            .catch(error => {
                Http.tratarErro(error, 'Algo deu errado...');
                onError(error);
            });
    }

    static post(url, data, onSuccess, onError, isFormData = false) {
        this.setClientUrl();
        if (isFormData) {
            instanceAxios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
        } else {
            instanceAxios.defaults.headers.common['Content-Type'] = 'application/json';
        }
        instanceAxios.post(url, data, {timeout: TIMEOUT})
            .then(response => {
                onSuccess(response.data);
            })
            .catch(error => {
                Http.tratarErro(error, 'Algo deu errado...');
                onError(error);
            });
    }

    static patch(url, data, onSuccess, onError, isFormData = false) {
        this.setClientUrl();
        if (isFormData) {
            instanceAxios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
        } else {
            instanceAxios.defaults.headers.common['Content-Type'] = 'application/json';
        }
        instanceAxios.patch(url, data, {timeout: TIMEOUT})
            .then(response => {
                onSuccess(response.data);
            })
            .catch(error => {
                Http.tratarErro(error, 'Algo deu errado...');
                onError(error);
            });
    }

    static tratarErro(error, title, message, arraybuttons) {
        if (!arraybuttons) {
            arraybuttons = [
                {
                    text: 'OK', onPress: () => {
                    }
                },
            ];
        }
        if (error.request.status === 404) {
            Alert.alert('Algo deu errado...', 'Código: 404', arraybuttons, {cancelable: false});
        } else if (error.request.status === 500) {
            Alert.alert('Algo deu errado...', 'Código: 500', arraybuttons, {cancelable: false});
        } else if (error.request.status === 403) {
            Alert.alert('Ops...', 'Você não tem permissão...', arraybuttons, {cancelable: false});
        } else if (error.response) {
            let titulo = '';
            let mensagem = '';

            if (error.response.data && !error.response.data.detail) {
                for (let key in error.response.data) {
                    if (error.response.data.hasOwnProperty(key)) {
                        if (error.response.data[key] && typeof error.response.data[key] !== 'string') {
                            if (error.response.data[key].length > 1) {
                                error.response.data[key].forEach((erro) => {
                                    titulo += (key.substr(0, 1).toUpperCase() + key.substr(1)) + '; ';
                                    mensagem += erro.replace('.', '') + '; ';
                                });
                            } else {
                                error.response.data[key].forEach((erro) => {
                                    titulo += (key.substr(0, 1).toUpperCase() + key.substr(1)) + '; ';
                                    mensagem += erro;
                                });
                            }
                        } else {
                            mensagem = error.response.data[key];
                            titulo = error.response.data[key];
                        }
                    }
                }
            } else {
                mensagem = error.response.data.detail;
                titulo = error.response.data.detail;
            }

            Alert.alert(
                ((title) ? title : titulo),
                ((message) ? message : mensagem),
                arraybuttons,
                {cancelable: false}
            );
        }
    }
}
