import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {View, Text, Container, Button, Content, Toast} from "native-base";
import {Image, TextInput, ActivityIndicator, Dimensions} from "react-native";
import {authenticate, cadastrar_usuario} from "../../redux/actions/auth";
import styles from './styles';

const {width} = Dimensions.get('window');
const HeaderLogo = require('../../assets/images/header_logo.png');

class CadastroScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password1: '',
            password2: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.usuario != null && !this.props.inicializando) {
            this.props.navigation.navigate('LoaderScreen');
        }
    }

    onClickCadastrar = async () => {
        if (this.props.autenticando) {
            Toast.show({
                text: 'Aguarde um instante...',
                buttonText: 'OK'
            });
            return;
        }
        if (!this.state.password1 || this.state.password1 == '') {
            Toast.show({
                text: 'A senha deve ter no mínimo 6 caracteres',
                buttonText: 'OK'
            });
            return;
        }
        if (this.state.password1 != this.state.password2) {
            Toast.show({
                text: 'As senhas não são iguais',
                buttonText: 'OK'
            });
            return;
        }

        this.props.cadastrar_usuario({username: this.state.username, email: this.state.email, password1: this.state.password1, password2: this.state.password2});
    };

    onClickCancelar = async () => {
        this.props.navigation.navigate('LoaderScreen');
    };

    render() {
        return (
            <Container style={styles.container}>
                <View>
                    <Image source={HeaderLogo}
                           style={{
                               padding: 0,
                               margin: 0,
                               resizeMode: 'contain',
                               width: width,
                               height: 55,
                               top: 0
                           }}/>
                </View>
                {this.props.autenticando || this.props.inicializando ?
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color="#ffffff"/>
                        <Text style={{color: 'white', textAlign: 'center'}}>Aguarde...</Text>
                    </View> :
                    <Content style={{padding: 30}}>
                        <View style={{flex: 0.1, marginBottom: 25}}>
                            <Text style={{textAlign: 'center', color: 'white', fontSize: 25}}>Cadastre-se</Text>
                        </View>
                        <View style={{flex: 0.7}}>
                            <TextInput style={styles.input} autoCapitalize="none"
                                       ref={(input) => this.usernameInput = input}
                                       onSubmitEditing={() => this.emailInput.focus()}
                                       onChangeText={(username) => this.setState({username})}
                                       value={this.state.username}
                                       autoCorrect={false}
                                       keyboardType='default'
                                       returnKeyType="next"
                                       placeholder='Apelido'
                                       placeholderTextColor='rgba(225,225,225,0.7)'/>
                            <TextInput style={styles.input} autoCapitalize="none"
                                       ref={(input) => this.emailInput = input}
                                       onSubmitEditing={() => this.password1Input.focus()}
                                       onChangeText={(email) => this.setState({email})}
                                       value={this.state.email}
                                       autoCorrect={false}
                                       keyboardType='email-address'
                                       returnKeyType="next"
                                       placeholder='E-mail'
                                       placeholderTextColor='rgba(225,225,225,0.7)'/>
                            <TextInput style={styles.input} autoCapitalize="none"
                                       ref={(input) => this.password1Input = input}
                                       onSubmitEditing={() => this.password2Input.focus()}
                                       onChangeText={(password1) => this.setState({password1})}
                                       value={this.state.password1}
                                       autoCorrect={false}
                                       keyboardType='default'
                                       returnKeyType="next"
                                       placeholder='Senha'
                                       placeholderTextColor='rgba(225,225,225,0.7)' secureTextEntry/>
                            <TextInput style={styles.input} autoCapitalize="none"
                                       ref={(input) => this.password2Input = input}
                                       onChangeText={(password2) => this.setState({password2})}
                                       value={this.state.password2}
                                       autoCorrect={false}
                                       keyboardType='default'
                                       returnKeyType="next"
                                       placeholder='Repita a senha'
                                       placeholderTextColor='rgba(225,225,225,0.7)' secureTextEntry/>
                        </View>
                        <View style={{flex: 0.2, flexDirection: 'row', justifyContent: 'center', paddingTop: 10}}>
                            <Button bordered block style={{
                                flex: 0.8,
                                marginRight: 10,
                                borderColor: '#b3b3b3',
                                borderRadius: 8,
                                height: 50
                            }} onPress={this.onClickCancelar.bind(this)}><Text
                                style={{color: 'white', textAlign: 'center'}}>CANCELAR</Text></Button>
                            <Button bordered block style={{
                                flex: 1.2,
                                backgroundColor: '#009538',
                                borderColor: '#b3b3b3', height: 50, borderRadius: 8,
                            }} onPress={this.onClickCadastrar.bind(this)}><Text
                                style={{color: 'white', textAlign: 'center'}}>CADASTRAR</Text></Button>
                        </View>
                    </Content>}
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({authenticate, cadastrar_usuario}, dispatch);

const mapStateToProps = state => ({
    usuario: state.authReducer.usuario,
    autenticando: state.authReducer.autenticando,
    inicializando: state.authReducer.inicializando
});

export default connect(mapStateToProps, mapDispatchToProps)(CadastroScreen);
